var log = require('npmlog');
var util = require('util');

function get_user_ext(ionicrequest, user_id) {
    var user = {};
    log.verbose("get_user_ext", "user_id= " + user_id);
    if(!user_id){
        log.info("get_user_ext", "user_id is empty. return no information.");
        return new Promise((resolve, reject) => { 
            return resolve(user);
        });
    }
    return new Promise((resolve, reject) => {
                log.verbose("get_user_ext", "launch ionicrequest");
        ionicrequest.get('/users/' + user_id, (error, res, body) => {
            if (!error && res.statusCode == 200) {
                user.userusername = extractUsernameFromUser(body.data);
                user.userpicture = extractPictFromUser(body.data);
                log.info("get_user_ext", "return user: " + user);
                return resolve(user);
            }
            else{
                log.error("get_user_ext", "unknown user_id.");
                log.error("get_user_ext", "Err Msg = " + error);
                return resolve(error);
            }
        });
    });
}

function errhook(req, res, next) {
    return function(err) {
        res.tmprep.errmsg = JSON.stringify(err);
        return next('SQL_ERROR');
    }
}
function getFilesizeInBytes(filename) {
    var stats = fs.statSync(filename)
    var fileSizeInBytes = stats["size"]
    return fileSizeInBytes
}
function setWhereClause(where, key, data){
    if(data != null){
        where[key] = data;
    }
}

function setWhereClause2(data){
    if(data != null){
        return data;
    }
}
function setsort(aggr, order, asc){
    if(order){
        log.verbose("func" ,"setsort");
        aggr.push({$sort: {order: asc}});
    }
}
function setlimit(aggr, limit, defaut){
    log.verbose("func" ,"setlimit");
    if(limit && limit < 99){
        aggr.push({$limit: limit});
    }
    else{
        aggr.push({$limit: defaut});
    }
}
function setoffset(aggr, offset, defaut){
    log.verbose("func" ,"setoffset");
    if(offset){
        aggr.push({$skip: offset});
    }
    else{
        aggr.push({$skip: defaut});
    }

}
function setrandom(aggr, limit, defaut){
    log.verbose("func" ,"setrandom");
    if(limit && limit < 99){
        aggr.push({$sample: {size: limit}});
    }
    else{
        aggr.push({$sample: {size: defaut}});
    }
}
function strToArray(str){
    return str.split(',');
}

function extractPictFromUser(_usr){
    let pict = '';
    if(_usr.social){
        if(_usr.social.facebook){
            pict = _usr.social.facebook.data.profile_picture;
        }
    }
    if(!pict){
        pict = _usr.details.image;
    }
        log.verbose("extractPictFromUser", "Picture returned is : "+ pict);
    return pict;

}
function extractUsernameFromUser(_usr){
    let username = '';
    if(_usr.social){
        if(_usr.social.facebook){
            username = _usr.social.facebook.data.username;
        }
    }
    if(!username){
        username = _usr.details.username;
    }
    log.verbose("extractUsernameFromUser", "Username returned is : "+ username);
    return username;
}

function explain(data){
    return util.inspect(data, { showHidden: true, depth: null });
}

exports.explain = explain;
exports.get_user_ext = get_user_ext;
exports.extractPictFromUser = extractPictFromUser;
exports.extractUsernameFromUser = extractUsernameFromUser;
exports.setrandom = setrandom;
exports.setsort = setsort;
exports.setlimit = setlimit;
exports.setoffset = setoffset;
exports.setWhereClause = setWhereClause;
exports.setWhereClause2 = setWhereClause2;
exports.getFilesizeInBytes = getFilesizeInBytes;
exports.errhook = errhook;
exports.strToArray = strToArray;
