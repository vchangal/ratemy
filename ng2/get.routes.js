var express = require('express');
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var ObjectID = mongodb.ObjectID;
var assert = require('assert');
var rymodels = require('./models');
var rycheck = require('./validate');
var ryutils = require('./utils');
var log = require('npmlog')
var _ = require('lodash')
const util = require('util');



/* Hiscores */
function init_getroutes(app, ionicrequest){
    log.info('init_getroutes');
    app.route('/api/v1.1/p/hiscores')
        .get(rycheck.validate_user_id('optional'))
        .get(rycheck.validate_order('opt'))
        .get(rycheck.validate_limit('get'))
        .get(rycheck.validate_offset('get'))
        .get(get_hiscores);


/* Resources */
app.route('/api/v1.1/p/resources*')
    .get(rycheck.validate_resource_id("get"))
    .get(rycheck.validate_user_id('get'))
    .get(rycheck.validate_limit('get'))
    .get(rycheck.validate_offset('get'));

app.route('/api/v1.1/p/resources')
    .get(rycheck.validate_order('opt'))
    .get(rycheck.validate_method('get'))
    .get(rycheck.validate_categories("get"))
    .get(rycheck.validate_type("get"))
    .get(rycheck.validate_coord_ne("get"))
    .get(rycheck.validate_coord_sw("get"))
    .get(get_resources);

app.route('/api/v1.1/p/resources/comments')
    .get(rycheck.validate_order('opt'))
    .get(get_comments);

app.route('/api/v1.1/p/users')
    .get(rycheck.validate_user_id('get'))
    .get(get_user);

app.route('/api/config')
    .get(get_config);
app.route('/api/v1.1/lang')
    .get(get_lang);
app.route('/api/v1.1/categories')
    .get(get_categories);

function get_hiscores(req, res, next) {
    var db = app.get('mongodb');
    log.info('REQUEST', 'get_hiscores');

    var hiscores = db.collection('users_ranking');
    var queryopt = {};
    var aggregation = [];
    var query = hiscores;
    if(res.tmpparams.user_id){
        log.verbose("get_hiscores", "user_id= " + res.tmpparams.user_id);
        queryopt.uuid = {$in: res.tmpparams.user_id};
    }
    aggregation.push({$match:queryopt});
            log.info(ryutils.explain(queryopt));
    ryutils.setsort(aggregation, res.tmpparams.order, 1);
    ryutils.setoffset(aggregation, res.tmpparams.offset, 0);
    ryutils.setlimit(aggregation, res.tmpparams.limit, 10);
    query.aggregate(aggregation).toArray(function (err, data){
        if(err){
            res.tmprep.errmsg = err;
            return next('BDD error');
        }
        res.tmprep.data = data;
        log.info('REQUEST', 'get_hiscores ended');
        return next();
    });
}

function get_comments(req, res, next) {
    log.info("get_comments");
    var db = app.get('mongodb');
    var resources = db.collection('resources');
    var queryopt = {};
    var aggregation = [];
    var query = resources;
    if(res.tmpparams.resource_id){
            queryopt['_id'] = {$in: res.tmpparams.resource_id};
    }
    if(res.tmpparams.user_id){
        queryopt['user_id'] = res.tmpparams.user_id;
    }
    aggregation.push({$match:queryopt});
    aggregation.push({$unwind: '$comments'});
    aggregation.push({$replaceRoot:{ newRoot: '$comments'}});
    ryutils.setoffset(aggregation, res.tmpparams.offset, 0);
    ryutils.setlimit(aggregation, res.tmpparams.limit, 10);
       aggregation.push({
        $addFields: {
            nbvotespos: {
                $size: { "$ifNull": [ {$filter: {
                    input: "$comments.votes",
                    as: "vote",
                    cond: { $eq: [ "$$vote.note", 1 ] }
                }}, []]},
            },
            nbvotesneg: {
                $size: { "$ifNull": [ {$filter: {
                    input: "$comments.votes",
                    as: "vote",
                    cond: { $eq: [ "$$vote.note", -1 ] }
                }}, []]},
            }
        }
    });
    log.verbose("get_comments", "aggregation= " + ryutils.explain(aggregation));
    query.aggregate(aggregation).toArray(function (err, data){
        log.info(ryutils.explain(data));
        if(err){
            res.tmprep.errmsg = err;
            return next('BDD error');
        }
        else{
            log.verbose("get_comments", "nb data = " + data.length);
            log.verbose("get_comments", "retrieve comments info");
            res.tmprep.data = data;
        }
        return next();
    });
}

function get_resources(req, res, next) {
    log.info("get_resources");
    var db = app.get('mongodb');
    var resources = db.collection('resources');
    var cats = db.collection('categories');
    var queryopt = {category_id:{}};
    var aggregation = [];
    if(res.tmpparams.resource_id){
            queryopt['_id'] = {$in: res.tmpparams.resource_id};
    }
    if(res.tmpparams.type){
        queryopt['type'] = {$in:res.tmpparams.type};

    }
    if(res.tmpparams.user_id){
        queryopt['user_id'] = {$in: res.tmpparams.user_id};
    }

	if(res.tmpparams.coord_ne && res.tmpparams.coord_sw){
		queryopt['location'] = {
            $geoWithin: {
                $box: [
                    res.tmpparams.coord_sw,
                    res.tmpparams.coord_ne
                ]
            }
        };
	}

    cats.find({private: {$ne:true}}).toArray(function (errcat, allowedcats){
        var extractcatsid = _.map(allowedcats, function (c){
            return c._id;
        });
        if(errcat){
            res.tmprep.errmsg = errcat;
            return next('BDD error');
        }
        queryopt['category_id'] = {
            $in: extractcatsid
        }
        if(res.tmpparams.category_id){
            queryopt['category_id']['$in'] = queryopt['category_id']['$in'].push(res.tmpparams.category_id);
        }
        var query = resources;
        aggregation.push({$match:queryopt});
        ryutils.setsort(aggregation, res.tmpparams.order, 1);
        if(res.tmpparams.method == 'random'){
            ryutils.setrandom(aggregation, res.tmpparams.limit, 10);
        }else{
            ryutils.setoffset(aggregation, res.tmpparams.offset, 0);
            ryutils.setlimit(aggregation, res.tmpparams.limit, 10);
        }
        log.verbose("get_resources", "aggregation= " + ryutils.explain(aggregation));
        query.aggregate(aggregation).toArray(function (err, data){
            if(err){
                res.tmprep.errmsg = err;
                log.error("get_resources", err);
                return next('BDD error');
            }
            else{
                log.verbose("get_resources", "nb data = " + data.length);
                log.verbose("get_resources", "retrieve user info");
                res.tmprep.data = data;
            }
            return next();
        });
    });
}

function get_user(req, res, next) {
    log.info("get_user");
    if(res.tmpparams.user_id){
        log.verbose("get_user", "user_id= " + res.tmpparams.user_id);
    }
    var arrayOfUser = _.map(res.tmpparams.user_id, (uid) => {
        return ryutils.get_user_ext(ionicrequest, uid).then((user) =>{
            return user;
        });
    });
    Promise.all(arrayOfUser).then(values => {
          console.log(values);
          res.tmprep.data = values;
          return next();
    })
    .catch(errors => {
        res.tmprep.errmsg = "one user_id cannot be fetched "+ errors;
        log.error("get_user", res.tmprep.errmsg);
        return next('BAD PARAMS');
    });
}
function get_config(req, res, next) {
    var db = app.get('mongodb');
    var config = db.collection('config');
    var queryopt = {};
    var query = config.find(queryopt);
    query.toArray(function (err, data){
        if(err){
            res.tmprep.errmsg = err;
            return next('BDD error');
        }
        res.tmprep.data = data;
        return next();
    });
}
function get_lang(req, res, next) {
    var db = app.get('mongodb');
    var lang = db.collection('lang');
    var queryopt = {};
    var query = lang.find(queryopt);
    query.toArray(function (err, data){
        if(err){
            res.tmprep.errmsg = err;
            return next('BDD error');
        }
        res.tmprep.data = data;
        return next();
    });
}
function get_categories(req, res, next) {
    var db = app.get('mongodb');
    var categories = db.collection('categories');
    var queryopt = {};
    queryopt['private'] = {$ne: true};
    log.info('func', "get_categories");
    var query = categories.find(queryopt);
    query.toArray(function (err, data){
        if(err){
            res.tmprep.errmsg = err;
            return next('BDD error');
        }
        res.tmprep.data = data;
        return next();
    });
}
}

/* exports */
exports.init_getroutes = init_getroutes;
