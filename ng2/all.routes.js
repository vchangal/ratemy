var request = require('request');
var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var rycheck = require('./validate');
var ryutils = require('./utils');
var ryconf = require('./config');
var cors = require('cors');
var init_setroutes = require('./set.routes').init_setroutes;
var init_getroutes = require('./get.routes').init_getroutes;
var log = require('npmlog');
log.level = 'verbose';

var app = express();

app.use(set_headers);
app.use(cors());
app.use(bodyParser.json({limit:'5mb'}));
app.use(expressValidator({
    customSanitizers: {
        defaulted: function(value, dft) {
            var newValue = dft;
            if(typeof(value) !== 'undefined'){
                newValue = value;
            }
            return newValue;
        }
    },
    customValidators: {
        isIntArray: function(value) {
            var i = 0;
            function isNormalInteger(str) {
                var n = ~~Number(str);
                return String(n) === str && n >= 0;
            }
            if(!Array.isArray(value)){
                return false;
            }
            for(i = 0; i < value.length;i++){
                if(!isNormalInteger(value[i])){
                    return false;
                }

            }
            return Array.isArray(value);
        },
        isArray: function(value) {
            return Array.isArray(value);
        },
        gte: function(param, num) {
            return param >= num;
        },
        isValidObjectID: function(param) {
            return ObjectID.isValid(param);
        },
    }
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.all('/api/*', init_api);
app.post('/api/v1.1/p/*', rycheck.validate_uuid());
//app.all('/api/v1.1/p/*', rycheck.validate_token);
app.post('/api/v1.1/p/*', islogged);

// Add headers
function set_headers(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods',
                  'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers',
                  'X-Requested-With,content-type, Authorization, uuid');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    return next();
}

function islogged(req, res, next) {
    log.verbose("func" ,"islogged");
    //res.tmpparams.uuid = req.get('uuid');
    ryconf.ionicrequest.get("users/" + res.tmpparams.uuid,
                     function (error, response, body) {
        if (!error && response.statusCode == 200) {
            return next();
        }
        else{
            return next('NOT_LOGGED');
        }
    });
}
function init_api(req, res, next){
    res.tmprep = {}; // contains the built response to send
    res.tmpparams = {}; //contains parameters validated and sanitized
    next();
}

init_setroutes(app, ryconf.ionicrequest);
init_getroutes(app, ryconf.ionicrequest);
app.all('/api/*', function(req, res, next){
    res.json(res.tmprep);
});
app.use(errorHandler);
function errorHandler(err, req, res, next) {
    log.error(err);
    if (res.headersSent) {
        return next(err);
    }
    res.status(500);
    if(!res.tmprep){
        res.tmprep = {};
    }
    res.tmprep.err = true;
    res.tmprep.errlabel = err;
    res.json(res.tmprep);
}
/* exports */
exports.app = app;
