var log = require('npmlog');
var util = require('util');
var ryconf = require('./config');
var ryutils = require('./utils');
var _ = require('lodash');

ryconf.openbd().then(db => {
    return get_users_list().then(users => {
        return Promise.all(users.map(computeUserScore(db)))
        .then(() => db.close());
    })
    .catch(errlog);
})
.catch(errlog);

/****
 * functions
 *
 * */
function get_users_list(){
    return new Promise((resolve, reject) => {
        ryconf.ionicrequest.get("users", (error, response, body) => {
            if (!error && response.statusCode == 200) {
                var users = _.map(body.data, usr => {
                    return {
                        uuid: usr.uuid,
                        username: ryutils.extractUsernameFromUser(usr),
                        picture: ryutils.extractPictFromUser(usr),
                    };
                });
                //log.info(ryutils.explain(users));
                resolve(users);
            }
            else
                reject(error);
        });

    });
}

function errlog(err) {
    log.error(err);
}
function computeUserTotal(usr){

    return 20 * usr.nbres +
        2 * usr.nbvotes +
        2 * usr.nbfavorites +
        1 * usr.nbviews +
        100 * usr.nbavg;
}
function computeUserScore(db){
    return (usr) => {
        var rank = db.collection('users_ranking');
        var res = db.collection('resources');
        return res.aggregate([
            {$match: {user_id: usr.uuid}},
            {$group: {
                _id: "$user_id",
                nbres: {$sum: 1},
                nbavg: {$sum: '$avg'},
                nbvotes: {$sum: {$size: { '$ifNull': ['$votes', []]}}},
                nbfavorites: {$sum: {$size: { '$ifNull': ['$favorites', []]}}},
                nbshares: {$sum: {$size: { '$ifNull': ['$shares', []]}}},
                nbviews: {$sum: {$size: { '$ifNull': ['$views', []]}}},

            } }
        ]).toArray().then(reslist => {
            var _res = reslist[0];
            log.info(ryutils.explain(reslist));
            if(!_res){
                usr.nbres = 0;
                usr.nbvotes = 0;
                usr.nbfavorites = 0;
                usr.nbviews = 0;
                usr.nbavg = 0;
            }else{
                usr.nbres = _res.nbres || 0;
                usr.nbvotes = _res.nbvotes || 0;
                usr.nbfavorites = _res.nbfavorites || 0;
                usr.nbviews = _res.nbviews || 0;
                usr.nbavg = _res.nbavg || 0;
            }
            usr.total = computeUserTotal(usr);
            rank.update({_id: usr.uuid}, usr, {upsert:true}).catch(errlog);
        })
        .catch(errlog);
    }
}
