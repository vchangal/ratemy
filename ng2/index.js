var MongoClient = require('mongodb').MongoClient;
var app = require('./all.routes').app;
var assert = require('assert');
var log = require('npmlog')

function init(){
    var db = app.get('mongodb');
    var config = db.collection('config');
    var categories = db.collection('categories');
    return config.findOne({name: 'init'}).then(function(init){
        if(init !== null){
            log.info("Already initialized");
            return;
        }
        log.info("launch init");

        config.insertMany([
            { name: 'init', value: 'true'},
            { name: 'version', value: '1.1'},
            { name: 'api_addr', value: 'https://ratemy2.changala.fr'},
            { name: 'data_addr', value: 'http://data.changala.fr:8080'},
            { name: 'url_config', value: '/https://ratemy2.changala.fr/api/config'},
            { name: 'url_login', value: 'https://ratemy2.changala.fr/api/v1.1/login'},
            { name: 'url_lang', value: '/https://ratemy2.changala.fr/api/v1.1/lang'},
            { name: 'url_categories', value: 'https://ratemy2.changala.fr/api/v1.1/categories'},
            { name: 'url_users', value: 'https://ratemy2.changala.fr/api/v1.1/p/users'},
            { name: 'url_hiscores', value: 'https://ratemy2.changala.fr/api/v1.1/p/hiscores'},
            { name: 'url_resources', value: 'https://ratemy2.changala.fr/api/v1.1/p/resources'},
            { name: 'url_resources_views', value: 'https://ratemy2.changala.fr/api/v1.1/p/resources/views'},
            { name: 'url_resources_votes', value: 'https://ratemy2.changala.fr/api/v1.1/p/resources/votes'},
            { name: 'url_resources_shares', value: 'https://ratemy2.changala.fr/api/v1.1/p/resources/shares'},
            { name: 'url_resources_favorites', value: 'https://ratemy2.changala.fr/api/v1.1/p/resources/favorites'},
            { name: 'url_resources_reports', value: 'https://ratemy2.changala.fr/api/v1.1/p/resources/reports'},
            { name: 'url_resources_comments', value: 'https://ratemy2.changala.fr/api/v1.1/p/resources/comments'},
            { name: 'url_comments_votes', value: 'https://ratemy2.changala.fr/api/v1.1/p/comments/votes'},
            { name: 'default_avatar', value: 'http://data.changala.fr:8080/default_avatar.png'}
        ]).then(function(r) {
            log.info("config initialized");

            categories.insertMany([
                { name: 'Cats', image: 'http://data.changala.fr:8080/img_cat/cat.jpg'},
                { name: 'Dogs', image: 'http://data.changala.fr:8080/img_cat/dog.jpg'},
                { name: 'Cars', image: 'http://data.changala.fr:8080/img_cat/car.jpg'},
            ]).then(function(r) {
                log.info("categories initialized");
            });
        }).catch(function(err){
            log.info(err);
        });
    });

}
var LE = require('letsencrypt-express');

    //server: 'staging',
var lex = LE.create({
    server: 'https://acme-v01.api.letsencrypt.org/directory',
    //configDir: '/var/www/ratemy/ng/letsencrypt/etc',
    //webrootPath: '/var/www/ratemy/ng/.well-known/acme-challenge',
    //store: require('le-store-certbot').create({ webrootPath: '/var/www/ratemy/ng/.well-known/acme-challenge' }),
    approveDomains: ['ratemy2.changala.fr'],
    renewWithin: 10 * 24 * 60 * 60 *1000,
    renewBy: 5 * 24 * 60 * 60 * 1000,
    email: 'vincent.changala@gmail.com',
    agreeTos: true,
    debug: true,
    app: app
});
// Connection URL
var url = 'mongodb://127.0.0.1:27017/ratemy';

// Use connect method to connect to the server
MongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    app.set('mongodb', db);
    init();
    log.info('init done');

    lex.listen();
    /*app.listen(3000, function(){
        log.info("Connected successfully to server");
    });
*/
});

exports.lex = lex;
