var log = require('npmlog');
var util = require('util');
var ryconf = require('./config');
var ryutils = require('./utils');
var _ = require('lodash');

/***
 * Script
 */
ryconf.openbd().then(db => {
    return run(db).then(() => db.close()).catch(errlog);
}).catch(errlog);

function computeRes(db){
    return (res) => {
        var nbVote;
        var nbView;
        var nbVotePos=0;
        var nbVoteNeg=0;
        var nbVoteNeu=0;
        var avg;

        if(typeof res.votes !== 'undefined'){
            nbVote = res.votes.length || 0;
            for(var i =0;i<nbVote;i++){
                if(res.votes[i].note === 0){
                    nbVoteNeu++;
                }else if(res.votes[i].note >= 1){
                    nbVotePos++;
                }else if(res.votes[i].note < 0){
                    nbVoteNeg++;
                }
            }
        }else{
            nbVote = 0;
        }
        if(typeof res.views !== 'undefined'){
            nbView = res.views.length;
        }else{
            nbView = 0;
        }
        if(nbVote)
            avg = (nbVotePos - nbVoteNeu)/ nbVote;
        else
            avg = '';

        return db.collection('resources').update(
            {"_id": res._id},
            {"$set": {
                "nb_votes": nbVote,
                "nb_views": nbView,
                avg: (nbVotePos - nbVoteNeu)/ nbVote,
                "nb_vote_pos":nbVotePos,
                "nb_vote_neu":nbVoteNeu,
                "nb_vote_neg":nbVoteNeg}}
        ).then(() => {
            log.info(ryutils.explain(res));
        }).catch(errlog);
    }
}
function run(db){
    var resource = db.collection('resources');
    var tabResources = resource.find().toArray();
    return tabResources.then(reslist => {
        return Promise.all(reslist.map(computeRes(db)));
    });
}

function errlog(err) {
    log.error(err);
}

