var ryutils = require('./utils');
var ObjectID = require('mongodb').ObjectID;
var mime = require('mime');
var child_process = require('child_process');
var gifsicle = require('gifsicle');
var im = require('imagemagick');
var fs = require("fs"); //Load the filesystem module
var log = require('npmlog')
var validator = require('validator');
var xss = require('xss');

log.level = 'verbose';

function validate_pass1(req, res, next) {
    log.verbose("func" ,"validate_pass1");
    req.assert('pass1', 'Required pass1').isLength({min:1});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        return next('PASSWORD');
    }
    else{
        res.tmpparams.pass1 = req.sanitize('pass1').escape();
    }
    return next();
}
function validate_title(opt) {
    return function (req, res, next) {
    log.verbose("func" ,"validate_title");
        if(!opt){
            req.assert('title', 'Unexpected name [4, 128]').isLength({min:4, max:128});
        }else{
            req.assert('title', 'Unexpected name [4, 128]')
                .optional()
                .isLength({min:4, max:128});
        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            return next('BAD_PARAM');
        }
        else{
            res.tmpparams.title = req.sanitize('title').escape();
        }
        return next();
    }
}
function validate_share(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_share");
        if(!opt){
            req.assert('share', 'Unexpected share [4, 50]').isLength({min:4, max:50});
        }else{
            req.assert('share', 'Unexpected share [4, 50]')
                .optional()
                .isLength({min:4, max:50});
        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            return next('BAD_PARAM');
        }
        else{
            res.tmpparams.share = req.sanitize('share').escape();
        }
        return next();
    }
}
function validate_name(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_name");
        if(!opt){
            req.assert('name', 'Unexpected name [4, 50]').isLength({min:4, max:50});
        }else{
            req.assert('name', 'Unexpected name [4, 50]')
                .optional()
                .isLength({min:4, max:50});
        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            return next('BAD_PARAM');
        }
        else{
            res.tmpparams.name = req.sanitize('name').escape();
        }
        return next();
    }
}
function validate_text(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_text");
        var text = req.body.text;
        if(!text && opt){
            log.verbose("validate_text" ,"data empty and optional");
            return next();
        }
        else{
            if(!validator.isLength(text, {min:0, max:255})){
                res.tmprep.errmsg = "unexpected text not between 0 and 255 characters";
                return next('BAD_PARAM');
            }else{
                res.tmpparams.text = xss(text);
            }
        }
        return next();
    }
}
function validate_username(req, res, next) {
    log.verbose("func" ,"validate_username");
    req.assert('username', 'Required username').notEmpty();
    req.assert('username', 'Invalid username').isAlphanumeric();
    //req.assert('username', 'Invalid username').matches(/^\w+$/);
    req.assert('username', '4 to 30 characters required').len(4, 30);
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        return next('username');
    }
    else{
        res.tmpparams.username = req.sanitize('username');
    }
    return next();
}

function validate_reason(req, res, next) {
    log.verbose("func" ,"validate_reason");
    req.assert('reason', 'Unexpected reason')
    .isInt({min: 0});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in reason parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.reason = req.sanitize('reason').toInt();
    return next();
}
function validate_limit(opt) {
    return (req, res, next) => {
        log.verbose("func" ,"validate_limit");
        var limit = req.query.limit;
        if(!limit && opt){
            log.verbose("validate_limit" ,"data empty and optional");
            return next();
        }
        if(!validator.isInt(limit)){
            res.tmprep.errmsg = "error in the limit parameter";
            return next('BAD_PARAM');
        }
        res.tmpparams.limit = validator.toInt(limit);
        log.verbose("validate_limit" ,"limit=" + res.tmpparams.limit);
        return next();
    }
}
function validate_uuid(opt) {
    return function (req, res, next) {
        log.verbose("VALIDATION" ,"validate_uuid");
        var uuid = req.get('uuid');
        if(!uuid && opt){
            log.verbose("validate_uuid" ,"data empty and optional");
            return next();
        }
        if(!uuid || !validator.isUUID(uuid)){
            res.tmprep.errmsg = "error in the uuid parameter";
            log.warn("validate" ,res.tmprep.errmsg);
            return next('BAD_PARAM');
        }
        res.tmpparams.uuid = uuid;
        log.verbose("VALIDATION" ,"validate_uuid succeed");
        return next();
    }
}
function validate_user_id(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_user_id");
        var user_id;
        if(opt)
           user_id = req.query.user_id;
       else
           user_id = req.body.user_id;

        if(!user_id && opt){
            log.verbose("validate_user_id" ,"data empty and optional");
            return next();
        }
        else if(opt == 'get'){
            user_id = ryutils.strToArray(user_id);
            for(i = 0; i < user_id.length; ++i){
                if(!validator.isUUID(user_id[i])){
                    res.tmprep.errmsg = "error in one of the user_id parameters";
                    return next('BAD_PARAM');
                }
            }
        }
        else{
            if(!validator.isUUID(user_id)){
                res.tmprep.errmsg = "error in the user_id parameter";
                return next('BAD_PARAM');
            }
        }
        res.tmpparams.user_id = user_id;
        log.verbose("func" ,"validate_user_id end");
        return next();
    }
}
function validate_categories(opt) {
    return function (req, res, next) {
        log.verbose("validate_categories", "validate_categories");
        var category_id;
        if(opt)
           category_id = req.query.category_id;
       else
           category_id = req.body.category_id;
        log.verbose("validate_categories", "validate_categories: " + category_id);
        if(!category_id && opt){
            log.verbose("validate_category_id" ,"data empty and optional");
            return next();
        }
        else if(opt == 'get'){
            category_id = ryutils.strToArray(category_id);
            for(i = 0; i < category_id.length; ++i){
                if(!validator.isMongoId(category_id[i])){
                    res.tmprep.errmsg = "error in one of the category_id parameters";
                    return next('BAD_PARAM');
                }
                category_id[i] = new ObjectID(category_id[i]);
            }
        }
        else{
            if(!validator.isMongoId(category_id)){
                res.tmprep.errmsg = "error in the category_id parameter";
                return next('BAD_PARAM');
            }
            res.tmpparams.category_id = new ObjectID(category_id);
        }
        log.verbose("func" ,"validate_category_id end");
        return next();
    }
}
function validate_creationdate(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_creationdate");
        var date = req.param('creationdate');
        if(opt && !date){
            log.verbose("validate_creationdate" ,"data empty and optional");
            return next();
        }
        else{
            date = validator.toDate(date);
            if(!opt && res.tmpparams.creationdate == null){
                res.tmprep.errmsg = "creationdate shall be mandatory but is null.";
                return next('BAD_PARAM');
            }else{
                res.tmpparams.creationdate = new Date(date);
            }
        }
        return next();
    }
}
function validate_type(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_type");
        var type;
        if(opt)
           type = req.query.type;
       else
           type = req.body.type;
        valcheck = [0, 1];
        if(!type && opt){
            log.verbose("validate_type" ,"data empty and optional");
            return next();
        }
        else if(opt=='get'){
            type = ryutils.strToArray(type);
            for(i = 0; i < type.length; ++i){
                if(!(validator.isIn(type[i], valcheck))){
                    res.tmprep.errmsg = "error in one of the type parameters";
                    return next('BAD_PARAM');
                }
                else{
                    type[i] = validator.toInt(type[i]);
                }
            }
            res.tmpparams.type = type;
        }
        else{
            if(!(validator.isInt(type, valcheck))){
                res.tmprep.errmsg = "unexpected type: "+ type;
                log.error("validate_type", res.tmprep.errmsg);
                return next('BAD_PARAM');
            }
            res.tmpparams.type = validator.toInt(type);
        }
        return next();
    }
}
function validate_token(req, res, next) {
    log.verbose("func" ,"validate_token");
    req.checkHeaders('authorization', 'Unexpected token').isLength({min:1});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in token parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.token = req.sanitizeHeaders('authorization').escape();
    return next();
}
function validate_offset(opt) {
    return (req, res, next) => {
        log.verbose("func" ,"validate_offset");
        var offset = req.query.offset;
        if(!offset && opt){
            log.verbose("validate_offset" ,"data empty and optional");
            return next();
        }
        if(!validator.isInt(offset)){
            res.tmprep.errmsg = "error in the offset parameter";
            return next('BAD_PARAM');
        }
    res.tmpparams.offset = validator.toInt(offset);
        log.verbose("validate_offset" ,"offset=" + res.tmpparams.offset);
        return next();
    }
}
function validate_coord_ne(opt){
    return (req,res,next) => {
        log.verbose("func", "validate_coordonnee_ne");
        var coord_border = req.query.coord_ne;
        if(!coord_border && opt){
            log.verbose("validate_coord_border", "data empty");
            return next();
        }
        var tableau = ryutils.strToArray(coord_border);
        res.tmpparams.coord_ne = tableau;
        return next();
    }
}
function validate_coord_sw(opt){
    return (req,res,next) => {
        log.verbose("func", "validate_coordonnee_sw");
        var coord_border = req.query.coord_ne;
        if(!coord_border && opt){
            log.verbose("validate_coord_border", "data empty");
            return next();
        }
        var tableau = ryutils.strToArray(coord_border);
        res.tmpparams.coord_sw = tableau;
        return next();
    }
}

function validate_method(opt) {
return (req, res, next) => {
    log.verbose("func" ,"validate_method");
    var method = req.query.method;
    if(!method && opt){
        log.verbose("validate_method" ,"data empty and optional");
        return next();
    }
    if(!validator.isIn(method, ['hiscores', 'random'])){
        res.tmprep.errmsg = "error in the method parameter";
        return next('BAD_PARAM');
    }
    res.tmpparams.method = method;
    log.verbose("validate_method" ,"method=" + res.tmpparams.method);
    return next();
}
}
function validate_comment_id(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_comment_id");
        res.tmpparams.comment_id = req.param('comment_id');
        if(!res.tmpparams.comment_id && opt){
            log.verbose("validate_comment_id" ,"data empty and optional");
            return next();
        }
        else if(opt == 'get'){
            res.tmpparams.comment_id = ryutils.strToArray(res.tmpparams.comment_id);
            for(i = 0; i < res.tmpparams.comment_id.length; ++i){
                if(!validator.isMongoId(res.tmpparams.comment_id[i])){
                    res.tmprep.errmsg = "error in one of the comment_id parameters";
                    return next('BAD_PARAM');
                }else{
                    res.tmpparams.comment_id[i] = new ObjectID(res.tmpparams.comment_id[i]);
                }
            }
        }
        else{
            if(!validator.isMongoId(res.tmpparams.comment_id)){
                res.tmprep.errmsg = "error in the comment_id parameters";
                return next('BAD_PARAM');
            }else{
                res.tmpparams.comment_id = new ObjectID(res.tmpparams.comment_id);
            }
        }
        log.verbose("func" ,"validate_comment_id end");
        return next();
    }
}
function validate_resource_id(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_resource_id");
        var resource_id;
        if(opt)
           resource_id = req.query.resource_id;
       else
           resource_id = req.body.resource_id;
        res.tmpparams.resource_id = resource_id;
        if(!res.tmpparams.resource_id && opt){
            log.verbose("validate_resource_id" ,"data empty and optional");
            return next();
        }
        else if(opt == 'get'){
            res.tmpparams.resource_id = ryutils.strToArray(res.tmpparams.resource_id);
            for(i = 0; i < res.tmpparams.resource_id.length; ++i){
                if(!validator.isMongoId(res.tmpparams.resource_id[i])){
                    res.tmprep.errmsg = "error in one of the resource_id parameters";
                    return next('BAD_PARAM');
                }else{
                    res.tmpparams.resource_id[i] = new ObjectID(res.tmpparams.resource_id[i]);
                }
            }
        }
        else{
            if(!validator.isMongoId(res.tmpparams.resource_id)){
                res.tmprep.errmsg = "error in the resource_id parameters";
                return next('BAD_PARAM');
            }else{
                res.tmpparams.resource_id = new ObjectID(res.tmpparams.resource_id);
            }
        }
        log.verbose("validate_resource_id",
                    "validated data is: " + res.tmpparams.resource_id);
        log.verbose("func" ,"validate_resource_id end");
        return next();
    }
}
function validate_commentsnote(req, res, next) {
    log.verbose("func" ,"validate_commentsnote");
    req.assert('note', 'Unexpected commentsnote')
    .isNumeric();
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in commentsnote parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.note = req.param('commentsnote');
    return next();
}
function validate_note(req, res, next) {
    log.verbose("func" ,"validate_note");
    req.assert('note', 'Unexpected note')
    .isInt({min: -5, max: 5});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in note parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.note = req.param('note');
    return next();
}
function validate_file(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_file");
        function gifsiclecallback(err) {
            if (err){
                res.tmprep.errmsg = err;
                return next('BAD_PARAM');
            }
            return next();
        };
        function writefile(err){
        }
        //var size = ryutils.getFilesizeInBytes(req.file.path);
        //var _mime = mime.lookup(req.file.path);
        var mimevalidation = ["image/jpeg", "image/gif"];
        req.assert('data', "data is note base64 encoded").isBase64();
        log.verbose("func" ,req.validationErrors(true));
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            res.tmprep.errmsg = "error in data parameters";
            log.verbose("func" ,"bad param");
            return next('BAD_PARAM');
        }
        if(typeof mimevalidation[res.tmpparams.type] === 'undefined'){
            res.tmprep.errmsg = "Type " + res.tmpparams.type + "is currently not supported";
            return next('BAD_PARAM');
        }
        res.tmpparams.data = req.param('data');
        res.tmpparams.file = {};
        res.tmpparams.file.tmppath = "./tmp/tmp_" + Date.now() + ".tmp";

        log.verbose("func", "pre writeFile");
        fs.writeFileSync(res.tmpparams.file.tmppath,
                         res.tmpparams.data, 'base64');
        log.verbose("func", "end writeFile");

        if(res.tmpparams.type == 0){
            res.tmpparams.file.filename = res.tmpparams.uuid + '_' + Date.now() + ".png";
            res.tmpparams.file.path = "/var/www/data/" + res.tmpparams.file.filename;
            log.verbose("func" ,"pre ip.resize");
            im.resize({
                srcPath: res.tmpparams.file.tmppath,
                dstPath: res.tmpparams.file.path,
                format: "png",
                width: 800,
                quality: 0.8
            }, function(err, stdout, stderr){
                if (err){
                    res.tmprep.errmsg = err;
                    return next('BAD_PARAM');
                }
                log.verbose("func" ,"resized");
                return next();
            });
        }else if(res.tmpparams.type == 1){
            res.tmpparams.file.filename = res.tmpparams.uuid + '_' + Date.now() + ".gif";
            res.tmpparams.file.path = "/var/www/data/" + res.tmpparams.file.filename;
            gifsicleopt = ['--optimize', '--resize-fit-width=800',
                '-o', req.tmpparams.path, res.tmpparams.tmppath];
            child_process.execFile(gifsicle, gifsicleopt, gifsiclecallback);
        }else{
            res.tmprep.errmsg = "type "+ res.tmpparams.type + " is not supported";
            log.warning("validate", res.tmprep.errmsg);
            return next();
        }
        /*if(size > 20 * 1024 * 1024){
            res.tmprep.errmsg = "FileSize too high (>20Mo)";
            return next('BAD_PARAM');
        }*/
    }
}

function validate_id(opt) {
    return function (req, res, next) {
    log.verbose("func" ,"validate_id");
        if(!opt){
            req.assert('id', 'Unexpected id')
                .isMongoId();
        }
        else{
            req.assert('id', 'Unexpected id')
                .optional()
                .isMongoId();

        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            res.tmprep.errmsg = "error in id parameters";
            return next('BAD_PARAM');
        }
        res.tmpparams.id = req.param('id');
        return next();
    }
}

function validate_order(opt) {
    return function (req, res, next) {
        log.verbose("func" ,"validate_order");
        var order = req.query.order;
        if(opt && !order){
            log.verbose("validate_order" ,"data empty and optional");
            return next();
        }
        else{
            if(!validator.isLength(order, {min:0, max:255})){
                res.tmprep.errmsg = "unexpected order not between 0 and 255 characters";
                return next('BAD_PARAM');
            }else{
                res.tmpparams.order = xss(order);
            }
        }
        return next();
    }
}


exports.validate_coord_ne = validate_coord_ne;
exports.validate_coord_sw = validate_coord_sw;
exports.validate_resource_id = validate_resource_id;
exports.validate_comment_id = validate_comment_id;
exports.validate_note = validate_note;
exports.validate_pass1 = validate_pass1;
exports.validate_categories = validate_categories;
exports.validate_method = validate_method;
exports.validate_username = validate_username;
exports.validate_id = validate_id;
exports.validate_user_id = validate_user_id;
exports.validate_limit = validate_limit;
exports.validate_type = validate_type;
exports.validate_creationdate = validate_creationdate;
exports.validate_offset = validate_offset;
exports.validate_uuid = validate_uuid;
exports.validate_token = validate_token;
exports.validate_text = validate_text;
exports.validate_name = validate_name;
exports.validate_share = validate_share;
exports.validate_title = validate_title;
exports.validate_file = validate_file;
exports.validate_commentsnote = validate_commentsnote;
exports.validate_reason = validate_reason;
exports.validate_order = validate_order;
