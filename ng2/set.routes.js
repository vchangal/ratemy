var express = require('express');
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var ObjectID = mongodb.ObjectID;
var assert = require('assert');
var expressValidator = require('express-validator');
var rycheck = require('./validate');
var log = require('npmlog');
var ryutils = require('./utils');
const util = require('util');
log.level = 'verbose';

function init_setroutes(app, ionicrequest){
app.route('/api/v1.1/p/resources/*')
    .post(rycheck.validate_resource_id());

app.route('/api/v1.1/p/resources')
    .post(rycheck.validate_id("optional"))
    .post(rycheck.validate_type())
    .post(rycheck.validate_creationdate("opt"))
    .post(rycheck.validate_text("optional"))
    .post(rycheck.validate_title("optional"))
    .post(rycheck.validate_categories())
    .post(rycheck.validate_file())
    .post(set_resources);

app.route('/api/v1.1/p/resources/views')
    .post(set_resources_views);
app.route('/api/v1.1/p/resources/votes')
    .post(rycheck.validate_note)
    .post(set_resources_votes);
app.route('/api/v1.1/p/resources/shares')
    .post(rycheck.validate_share("opt"))
    .post(set_resources_shares);
app.route('/api/v1.1/p/resources/favorites')
    .post(set_resources_favorites);
app.route('/api/v1.1/p/resources/reports')
    .post(rycheck.validate_text())
    .post(rycheck.validate_reason)
    .post(set_resources_reports);
app.route('/api/v1.1/p/resources/comments')
    .post(rycheck.validate_text())
    .post(set_resources_comments);
app.route('/api/v1.1/p/comments/votes')
    .post(rycheck.validate_commentsnote)
    .post(set_comments_votes);

app.route('/api/v1.1/p/resources/comments')
    .delete(rycheck.validate_comment_id())
    .delete(del_resources_comments);
app.route('/api/v1.1/p/resources/shares')
    .delete(del_resources_shares);
app.route('/api/v1.1/p/resources/favorites')
    .delete(del_resources_favorites);

app.route('/api/v1.1/login')
    .all(rycheck.validate_uuid())
    .post(login)
    .post(is_banned);

function login(req, res, next) {
    log.info("hook", "login hook");
    ionicrequest.get("users/" + res.tmpparams.uuid, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            return next();
        }
        else{
            return next("NOT_LOGGED");
        }
    });
}
function is_banned(req, res, next) {
    var db = app.get('mongodb');
    var banned = db.collection('banned');
    banned.count({uuid : res.tmpparams.uuid})
        .then(nb => {
            if(nb)
                return next("BANNED");
            return next();
        })
        .catch(err => {
            log.error(err);
            return next();
        });
}
function set_resources(req, res, next) {
    log.info("set_resources", "set_resources");
    var db = app.get('mongodb');

    var resources = db.collection('resources');
    var categories = db.collection('categories');

    ionicrequest.get("users/" + res.tmpparams.uuid, function (error, response, body) {
        log.verbose("set_resources", "error = " + error);
        if (!error && response.statusCode == 200) {
            log.verbose("set_resources", "find cat = " + res.tmpparams.category_id);
            categories.findOne({
                _id: new ObjectID(res.tmpparams.category_id)
            }).then(function(cat){
                log.verbose("set_resources", "insert resource = " + cat);
                var createdres = {
                    path : 'http://data.changala.fr:8080/' + res.tmpparams.file.filename,
                    type : res.tmpparams.type,
                    category_id : cat._id,
                    user_id : res.tmpparams.uuid,
                    creationdate : res.tmpparams.creationdate,
                    title : res.tmpparams.title,
                    description : res.tmpparams.text,
                };
                resources.insert(createdres).then(function(resres) {
                    log.info("set_resources", "resources inserted");
                    categories.updateOne({
                        _id: new ObjectID(res.tmpparams.category_id)
                    },
                    {
                        $inc: {number: 1}
                    }).then(function(c){
                        log.verbose("set_resources", "cat incremented");
                        res.tmprep.data = resres.ops[0];
                        return next();
                    }).catch(function(err){
                        res.tmprep.errmsg = err;
                        log.error("set_resources", err);
                        return next("BDD error");
                    });
                }).catch(function(err){
                    res.tmprep.errmsg = err;
                    log.error("set_resources", err);
                    return next("BDD error");
                });

            })
            .catch(function(err){
                res.tmprep.errmsg = err;
                log.error("func", err);
                return next("BDD error");
            });

        }
    });
}

function set_resources_views(req, res, next) {
    var db = app.get('mongodb');

    var resources = db.collection('resources');
    var newres = resources.findAndModify({
        _id: res.tmpparams.resource_id,
        'views.user_id': res.tmpparams.uuid
    },
    [],
    {
        $set: {
            'views.$.user_id': res.tmpparams.uuid
        },
        $inc:{
            'views.$.nb': 1
        }
    },
    {
        new:true
    }).then(function(ress){
        if(ress.lastErrorObject){
            if(ress.lastErrorObject.n == 0){
                log.info("set_resources_views","views was not existing. add it");
                resources.update({
                    _id: res.tmpparams.resource_id,
                },
                {
                    $addToSet: {
                        'views': {
                            user_id: res.tmpparams.uuid,
                            nb: 1
                        }
                    }
                }).then(function(ress){
                    res.tmprep.data = ress;
                    return next();
                }).catch(function(err){
                    res.tmprep.errmsg = err;
                    log.error("func", err);
                    return next("BDD error");
                });
            }
        }else{
            res.tmprep.errmsg = "unexpected internal result";
            log.error("func", res.tmprep.errmsg);
            return next("Internal error");
        }
        res.tmprep.data = ress;
        return next();
    }).catch(function(err){
        res.tmprep.errmsg = err;
        log.error("func", err);
        return next("BDD error");
    });
}

function set_comments_votes(req, res, next) {
    var db = app.get('mongodb');

    var resources = db.collection('resources');
    var newres = resources.updateOne({
        _id: new ObjectID(res.tmpparams.resource_id),
        comments: {
            _id: new ObjectID(res.tmpparams.comment_id)
        }
    },
    {
        $set: {"comments.$.note": res.tmpparams.note}
    });
    res.tmprep.data = newres;
    return next();
}

function set_resources_votes(req, res, next) {
    var db = app.get('mongodb');
    log.info("set_resources_votes", "func");
    var resources = db.collection('resources');
    var newres = resources.findAndModify({
        _id: res.tmpparams.resource_id,
        'votes.user_id': res.tmpparams.uuid
    },
    [],
    {
        $set: {
            'votes.$.user_id': res.tmpparams.uuid,
            'votes.$.note': res.tmpparams.note,
        }
    },
    {
        new:true
    }).then(function(ress){
        if(ress.lastErrorObject){
            if(ress.lastErrorObject.n == 0){
                log.info("set_resources_votes","votes was not existing. add it");
                resources.update({
                    _id: res.tmpparams.resource_id,
                },
                {
                    $addToSet: {
                        'votes': {
                            user_id: res.tmpparams.uuid,
                            note: res.tmpparams.note
                        }
                    }
                }).then(function(ress){
                    res.tmprep.data = ress;
                    return next();
                }).catch(function(err){
                    res.tmprep.errmsg = err;
                    log.error("func", err);
                    return next("BDD error");
                });
            }
        }else{
            res.tmprep.errmsg = "unexpected internal result";
            log.error("func", res.tmprep.errmsg);
            return next("Internal error");
        }
        res.tmprep.data = ress;
        return next();
    }).catch(function(err){
        res.tmprep.errmsg = err;
        log.error("func", err);
        return next("BDD error");
    });
}

function set_resources_shares(req, res, next) {
    var db = app.get('mongodb');
    log.info("set_resources_shares", "func");
    var resources = db.collection('resources');
    var newres = resources.findAndModify({
        _id: res.tmpparams.resource_id,
        'shares.user_id': res.tmpparams.uuid
    },
    [],
    {
        $set: {
            'shares.$.user_id': res.tmpparams.uuid,
            'shares.$.origin': res.tmpparams.share
        },
        $inc: {
            'shares.$.nb': 1
        }
    },
    {
        new:true
    }).then(function(ress){
        if(ress.lastErrorObject){
            if(ress.lastErrorObject.n == 0){
                log.info("set_resources_shares","shares was not existing. add it");
                resources.update({
                    _id: res.tmpparams.resource_id,
                },
                {
                    $addToSet: {
                        'shares': {
                            user_id: res.tmpparams.uuid,
                            origin: res.tmpparams.share,
                            nb: 1
                        }
                    }
                }).then(function(ress){
                    res.tmprep.data = ress;
                    return next();
                }).catch(function(err){
                    res.tmprep.errmsg = err;
                    log.error("func", err);
                    return next("BDD error");
                });
            }
        }else{
            res.tmprep.errmsg = "unexpected internal result";
            log.error("func", res.tmprep.errmsg);
            return next("Internal error");
        }
        res.tmprep.data = ress;
        return next();
    }).catch(function(err){
        res.tmprep.errmsg = err;
        log.error("func", err);
        return next("BDD error");
    });
}

function set_resources_reports(req, res, next) {
    var db = app.get('mongodb');

    var report = {
        text: res.tmpparams.text,
        reason: res.tmpparams.reason
    };
    var key = "reports."+res.tmpparams.uuid;
    var resources = db.collection('resources');
    var newres = resources.updateOne({
        _id: new ObjectID(res.tmpparams.resource_id)
    },
    { $set: {key: report}});
    res.tmprep.data = newres;
    return next();
}
function set_resources_comments(req, res, next) {
    var db = app.get('mongodb');
    var comment = {
        _id: new ObjectID(),
        user_id: res.tmpparams.uuid,
        text: res.tmpparams.text,
        updated: 1
    };
    var resources = db.collection('resources');
    var newres = resources.updateOne({
        _id: res.tmpparams.resource_id
    },
    { $push: {"comments": comment}}).then(result => {
        res.tmprep.data = result;
        return next();
        
    })
    .catch(err =>{
        res.tmprep.errmsg = err;
        log.error('BDD_ERROR', ryutils.explain(err))
        return next('BDD_ERROR');
        
    });
}
function set_resources_favorites(req, res, next) {
    var db = app.get('mongodb');
    log.info("set_resources_favorites", "func");
    var resources = db.collection('resources');
    var newres = resources.findAndModify({
        _id: res.tmpparams.resource_id,
        'favorites.user_id': res.tmpparams.uuid
    },
    [],
    {
        $set: {
            'favorites.$.user_id': res.tmpparams.uuid
        },
        $inc: {
            'favorites.$.nb': 1
        }
    },
    {
        new:true
    }).then(function(ress){
        if(ress.lastErrorObject){
            if(ress.lastErrorObject.n == 0){
                log.info("set_resources_favorites","favorites was not existing. add it");
                resources.update({
                    _id: res.tmpparams.resource_id,
                },
                {
                    $addToSet: {
                        'favorites': {
                            user_id: res.tmpparams.uuid,
                            nb: 1
                        }
                    }
                }).then(function(ress){
                    res.tmprep.data = ress;
                    return next();
                }).catch(function(err){
                    res.tmprep.errmsg = err;
                    log.error("func", err);
                    return next("BDD error");
                });
            }
        }else{
            res.tmprep.errmsg = "unexpected internal result";
            log.error("func", res.tmprep.errmsg);
            return next("Internal error");
        }
        res.tmprep.data = ress;
        return next();
    }).catch(function(err){
        res.tmprep.errmsg = err;
        log.error("func", err);
        return next("BDD error");
    });
}
function del_resources_favorites(req, res, next) {
    var db = app.get('mongodb');
    log.info("del_resources_favorites", "func");
    var resources = db.collection('resources');
    var newres = resources.findAndModify({
        _id: res.tmpparams.resource_id,
        'favorites.user_id': res.tmpparams.uuid
    },
    [],
    {
        $pull: {
            favorites: {user_id: res.tmpparams.uuid}
        },
    },
    {
        new:true
    }).then(function(ress){
        if(ress.lastErrorObject){
            if(ress.lastErrorObject.n == 0){
                log.info("del_resources_favorites","favorites was not existing. add it");
                    res.tmprep.data = {msg:"no favorite in db"};
            }
        }else{
            res.tmprep.errmsg = "unexpected internal result";
            log.error("func", res.tmprep.errmsg);
            return next("Internal error");
        }
        res.tmprep.data = ress;
        return next();
    }).catch(function(err){
        res.tmprep.errmsg = err;
        log.error("func", err);
        return next("BDD error");
    });
}
function del_resources_comments(req, res, next) {
    var db = app.get('mongodb');
    log.info("del_resources_comments", "func");
    var resources = db.collection('resources');
    var newres = resources.update({
        _id: res.tmpparams.resource_id,
    },
    {
        $pull: {
            comments: {_id: res.tmpparams.comment_id}
        },
    },
    { }).then(function(ress){
        log.verbose(ryutils.explain(ress));
        if(ress.lastErrorObject){
            if(ress.lastErrorObject.n == 0){
                log.info("del_resources_comments","comments was not existing. add it");
                    res.tmprep.data = {msg:"no share in db"};
            }
        }else{
            res.tmprep.errmsg = "unexpected internal result";
            log.error("func", res.tmprep.errmsg);
            return next("Internal error");
        }
        res.tmprep.data = ress;
        return next();
    }).catch(function(err){
        res.tmprep.errmsg = err;
        log.error("func", err);
        return next("BDD error");
    });
}
function del_resources_shares(req, res, next) {
    var db = app.get('mongodb');
    log.info("del_resources_shares", "func");
    var resources = db.collection('resources');
    var newres = resources.findAndModify({
        _id: res.tmpparams.resource_id,
        'shares.user_id': res.tmpparams.uuid
    },
    [],
    {
        $pull: {
            shares: {user_id: res.tmpparams.uuid}
        },
    },
    {
        new:true
    }).then(function(ress){
        if(ress.lastErrorObject){
            if(ress.lastErrorObject.n == 0){
                log.info("del_resources_shares","shares was not existing. add it");
                    res.tmprep.data = {msg:"no share in db"};
            }
        }else{
            res.tmprep.errmsg = "unexpected internal result";
            log.error("func", res.tmprep.errmsg);
            return next("Internal error");
        }
        res.tmprep.data = ress;
        return next();
    }).catch(function(err){
        res.tmprep.errmsg = err;
        log.error("func", err);
        return next("BDD error");
    });
}
}
/* exports */
exports.init_setroutes = init_setroutes;
