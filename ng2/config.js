var request = require('request');
var MongoClient = require('mongodb').MongoClient;


var ionicrequest = request.defaults({
      headers: {
          'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJhNjM1NTQ4NC1hYjk0LTRkN2QtYjljZC04M2ZjZGY1YTdhMDgifQ.rQK8w-3VGeTU1XMuEzPDyWgfd_1AKwf9RAUV-EdiHTo',
          'Content-Type': 'application/json'
      },
      baseUrl : "https://api.ionic.io/",
      json: true
});

function openbd(){
    var url = 'mongodb://127.0.0.1:27017/ratemy';
    return new Promise((resolve, reject) => {
        MongoClient.connect(url, (err, db) => {
            if(err)
                reject(err);
            else
                resolve(db);
        });

    });
}

exports.openbd = openbd;
exports.ionicrequest = ionicrequest;
