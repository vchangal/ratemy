<?php
// Use set_error_handler() to tell PHP to use our method
//require 'lib/flight/Flight.php';
require_once 'lib/flight/autoload.php';
require_once 'lib/medoo.php';
require_once 'lib/config.php';
require_once 'lib/validation.php';
require_once 'lib/validation2.php';
require_once 'lib/db.php';
ini_set('error_reporting', E_ALL);
ini_set('display_errors', '1');


use flight\Engine;
$db = new medoo(Config::$DB);
$app = new Engine();
$app->set('flight.log_errors', true);
$app->set('db', $db);
$app->set('BASE_DIR', __DIR__);
$app->set('answer', []);
$app->set('validated', []);

/**
 * Permet de tester si le username est disponible
 * @param username username a tester
 * @return indique si le username est disponible (http status)
 **/
$app->route('/test/is_username_available', function($route){
    $app = $route->app;
    $app->is_username_available();
    $app->send();
}, true);

/**
 * Permet de se deconnecter
 * @param username username pour se logout
 * @param token token pour se logout
 * @return
 **/
$app->route('POST /logout', function($route){
    $app = $route->app;
    list($username, $token) = $app->islogged();
    logout($app, $username, $token);
}, true);
/**
 * Permet d'obtenir la configuration
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @return la config
 **/
$app->route('GET /api/v1.0/config', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $cat = $db->select('config', '*');
    save_data($app, null, $cat);
    $app->send();
}, true);
/**
 * Permet de sélectionner des categories.
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @return les catégories
 **/
$app->route('GET /api/v1.0/categories', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $cat = $db->select('categories_augmented', '*');
    save_data($app, null, $cat);
    $app->send();
}, true);

/**
 * Permet de sélectionner des users.
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param user_id (optional) id ou tableau des ids pour lesquels
 * on veut voir les user
 * @param login true or false
 * @param user (optional) nom ou tableau des noms pour lesquels on veut voir les user
 * @param offset (optional) rang de l'resource a partir duquel on veut voir les
 * resources
 * @param limit (optional) nombre d'resources que l'on veut récupér
 * @param rank (optional) rang dans les hiscores des joueurs que l'on veut voir
 * @param sort (optional) compétence sur lesquelles on veut classer les joueurs
 *  valeurs possibles:
 * 'total', 'user_id', 'username',
 * 'picture', 'nb_votes', 'nb_views',
 * 'nb_shares', 'nb_reports', 'nb_favorites',
 * 'nb_comments', 'nb_resources', 'score'
 * valeur par defaut : total
 * @return les users
 **/
$app->route('GET /api/v1.0/users(/@id:[0-9]+)', function($id, $route){
    $app = $route->app;
    $db = $app->get('db');
    $resources = false;
    $login = $app->validate2('login', false);
    if($login){
        login($app);
    }
    $app->islogged();
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $user_id = $app->validate2('user_id', null);
    $user = $app->validate2('user', null);
    $rank = $app->validate2('rank', null);
    $sort = $app->validate2('sort', "total");
    $where = "1";
    if(!is_null($id)){
        $where = "T.user_id = $id";
    }
    else if(!is_null($user_id)){
        $where = "T.user_id in (".join(', ', $user_id).") ";
    }
    if(!is_null($user)){
        $where .= "and T.username in (".join(', ', $user).") ";
    }
    if(!is_null($rank)){
        $where .= "and T.rank in (".join(', ', $rank).") ";
    }
    $db->query("set @rank=0;");
    $query = $db->query("
        select T.* FROM (
            SELECT @rank := @rank +1 AS rank, users_hiscores.*
            FROM users_hiscores
            ORDER BY users_hiscores.$sort DESC
        ) AS T where $where
        LIMIT $offset, $limit;
        ");
    $resources = $query ? $query->fetchAll(PDO::FETCH_ASSOC) : false;
    if($resources === false){
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    $resources = (!is_null($id) && count($resources) >=1) ? $resources[0] : $resources;
    save_data($app, null, $resources);
    $app->send();
}, true);
/**
 * Permet de sélectionner des resources.
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id (optional) id ou tableau d'id des resources que l'on veut voir
 * @param user_id (optional) id ou tableau de user id pour lesquels
 * on veut voir les resources
 * @param offset (optional) rang de la resource a partir duquel on veut voir les
 * resources
 * @param limit (optional) nombre d'resources que l'on veut récupérer
 * @param type (optional) type de la resource (1=image)
 * @param method (optional) choisi comment les images sont ordonnées (par 
 * défaut dans l'ordre des notes. si "random" images envoyées dans l'ordre 
 * random)
 * @param categories (optional) id ou tableau d'id des catégores des resources
 * @param arrayexpected (optional, default value = true) indique si on veux un 
 * tableau ou un élément
 * @return les resources
 **/
$app->route('GET /api/v1.0/resources', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $resource_id = $app->validate2('resource_id', null);
    $user_id = $app->validate2('user_id', null);
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $cat = $app->validate2('categories', null);
    $type = $app->validate2('type', 0);
    $type = min($type, 5);
    $method = $app->validate2('method', "hiscores");
    $arrayexpected = $app->validate2('arrayexpected', true);
    $where = [];
       //TODO "ORDER" =>
    if(!is_null($user_id)){
        $where['user_id'] = $user_id;
    }
    if(!is_null($cat)){
        $where['category'] = $cat;
    }
    if(!is_null($type)){
        $where['type'] = $type;
    }
    if(!is_null($resource_id)){
        $where['id'] = $resource_id;
    }
    $cond = ["LIMIT" => [$offset, $limit]];
    if(!empty($where)){
        $cond["AND"] = $where;
    }
    $table = "resources_$method";
    $func = 'get';
    if($arrayexpected){
        $func = 'select';
    }
    $resources = $db->$func($table, '*', $cond);
    if($resources === false){
        error_log(print_r($db->error(),true));
    }else{
        save_data($app, null, $resources);
    }
    $app->send();
}, true);
/**
 * Permet d ajouter une resource
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param categories catégorie de l'resource
 * @param type type de la resource (0:images)
 * @param name nom de l'resource
 * @param description (optional) description de l'resource
 * @param file de $_FILES fichier a uploader
 * @return
 **/
$app->route('POST /api/v1.0/resources', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $category = $app->validate2('categories');
    $category = $category[0]; // Allowed only one category
    $cat_name = get_category_name($db, $category);
    $type = $app->validate2('type');
    $name = $app->validate2('name');
    $description = $app->validate2('description', "");
    $file = $app->validate2('file');
    /* creation du dossier */
    $dest = "resources/$cat_name/$username";
    if (!file_exists($dest)) {
        mkdir($dest, 0777, true);
    }
    list($path, $mime) = $app->savefile($file, $dest, time());
    $resource_id = $db->insert('resources',
        ['path' => $path,
        'description' => $description,
        'name' => $name,
        'type' => $type,
        'category' => $category,
        'mime' => $mime,
        'user_id' => $user_id]);
    if($resource_id == 0){
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    $app->send();
}, true);

/**
 * Permet de supprimer une resource
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de l'resource
 * @return
 **/
$app->route('DELETE /api/v1.0/resources', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id');
    $cond = ["AND" => ['id' => $resource_id, 'user_id' => $user_id]];
    $nb_resource_id = count($resource_id);
    $paths = $db->select("resources", "path", ["id" => $resource_id]);
    if (!file_exists($app->get('BASE_DIR') . '/resources/.trash')) {
        mkdir($app->get('BASE_DIR') . '/resources/.trash', 0777, true);
    }
    $nb_row = $db->count('resources', $cond);
    if($nb_row == $nb_resource_id){
        $nb_row = $db->delete('resources', $cond);
        if($nb_row == $nb_resource_id){
            for($i=0; $i < $nb_resource_id;$i++){
                rename($app->get('BASE_DIR') . $resource_id[$i],
                    $app->get('BASE_DIR') . '/resources/.trash/'.basename($resource_id[$i]));
            }
        }
        else{
            $app->send('RESOURCE_DELETE');
        }
    }else{
        $app->send('RESOURCE_DELETE');
    }
    $app->send();
}, true);

/**
 * Permet de s'enregistrer
 * @param username nécessaire pour prouver que l'on est connecté
 * @param token nécessaire pour prouver que l'on est connecté
 * @param pass1 password
 * c'est au client de vérifier que l'utilisateur a bien mis le mot de passe 
 * qu'il voulait
 * @param uuid identifiant
 * @return
 **/
$app->route('PUT /api/v1.0/users', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $password, $uuid) = $app->canregister();
    $last_user_id = $db->insert("users", [
        "username" => $username,
        "password" => $password,
        "uuid" => $uuid,
        "creationdate" => date("Y-m-j H:i:s")
        ]);
    login($app);
}, true);

/**
 * Permet de modifier un profile
 * @param username nécessaire pour prouver que l'on est connecté
 * @param token nécessaire pour prouver que l'on est connecté
 * @param pass1 (optional) nouveau password pour l'utilisateur. Attention
 * c'est au client de vérifier que l'utilisateur a bien mis le mot de passe qu'il voulait
 * @param mail (optional) adresse mail de l'utilisateur
 * @param file (optional) de $_FILES image de profil a uploader
 * @return
 **/
$app->route('POST /api/v1.0/users', function($id, $route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $pass1 = $app->validate2('pass1', null);
    $email = $app->validate2('mail', null);
    $file = $app->validate2('file');
    $dest = "profiles";
    list($path, $mime) = $app->savefile($file, $dest, time().$username);
    $to_update = [];
    if(!is_null($pass1)){
        $to_update["password"] = $pass1;
    }
    if(!is_null($mail)){
        $to_update["mail"] = $mail;
    }
    if(!is_null($picture)){
        $to_update["picture"] = $path;
    }
    $last_user_id = $db->update("users", $to_update,
        ["AND" => [
        "id" => $user_id,
    ]]);
    if(!$last_user_id){
        error_log(print_r($db->error(),true));
        save_data($app, 'msg', "sql error", true);
        $app->send('DB');
    }
    $app->send();
}, true);

/**
 * Permet de noter une resource
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de l'resource
 * @param note note de l'resource [0-5]
 * @return
 **/
$app->route('POST /api/v1.0/resources/vote', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $note = $app->validate2('vote');
    $resource_id = $app->validate2('resource_id');
    $cond = ["AND" => [
        'user_id' => $user_id,
        'resource_id' => $resource_id[0]]
    ];
    $has_voted = $db->count('votes', $cond);
    if($has_voted){
        $db->update('votes', ['note' => $note], $cond);
    }
    else{
        $vote_id = $db->insert('votes', ['user_id' => $user_id,
            'resource_id' => $resource_id[0],
            'note' => $note]);
        if($vote_id == 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    save_data($app, "username", $username);
    save_data($app, "token", $token);
    save_data($app, "resource_id", $resource_id[0]);
    save_data($app, "user_id", $user_id);
    save_data($app, "note", $note);
    $app->send();
}, true);
/**
 * Permet de connaitre le nombre de votes
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id (optional) id ou tableau d'id des resources que l'on veut voir
 * @param user_id (optional) id ou tableau d'id pour lesquels
 * on veut voir les resources
 * @param offset (optional) rang 
 * @param limit (optional) nombre d'element
 * @param arrayexpected (optional, default value = true) indique si on veux un 
 * tableau ou un élément
 * @return les votes
 **/
$app->route('GET /api/v1.0/resources/vote', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $resource_id = $app->validate2('resource_id', null);
    $user_id = $app->validate2('user_id', null);
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $arrayexpected = $app->validate2('arrayexpected', true);
    $where = [];
       //TODO "ORDER" =>
    if(!is_null($user_id)){
        $where['user_id'] = $user_id;
    }
    if(!is_null($resource_id)){
        $where['resource_id'] = $resource_id;
    }
    $func = 'get';
    if($arrayexpected){
        $func = 'select';
    }
    $cond = ["LIMIT" => [$offset, $limit]];
    if(!empty($where)){
        $cond["AND"] = $where;
    }
    $votes = $db->$func('votes', '*', $cond);
    if($votes === false && $func == "select"){
        save_data($app, 'msg', "sql error $cond", true);
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    else if($votes === false && $func == "get"){
        save_data($app, null, ["id" => 0]);
    }else{
        save_data($app, null, $votes);
    }
    $app->send();
}, true);


/**
 * Permet de connaitre le nombre de vues
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id (optional) id ou tableau d'id des resources que l'on veut voir
 * @param user_id (optional) id ou tableau d'id pour lesquels
 * on veut voir les resources
 * @param offset (optional) rang 
 * @param limit (optional) nombre d'element
 * @param arrayexpected (optional, default value = true) indique si on veux un 
 * tableau ou un élément
 * @return les vues
 **/
$app->route('GET /api/v1.0/resources/view', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $resource_id = $app->validate2('resource_id', null);
    $user_id = $app->validate2('user_id', null);
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $arrayexpected = $app->validate2('arrayexpected', true);
    $where = [];
       //TODO "ORDER" =>
    if(!is_null($user_id)){
        $where['user_id'] = $user_id;
    }
    if(!is_null($resource_id)){
        $where['resource_id'] = $resource_id;
    }
    $func = 'get';
    if($arrayexpected){
        $func = 'select';
    }
    $cond = ["LIMIT" => [$offset, $limit]];
    if(!empty($where)){
        $cond["AND"] = $where;
    }
    $views = $db->$func('views', '*', $cond);
    if($views === false && $func == "select"){
        save_data($app, 'msg', "sql error $cond", true);
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    else if($views === false && $func == "get"){
        save_data($app, null, ["id" => 0]);
    }else{
        save_data($app, null, $views);
    }
    $app->send();
}, true);

/**
 * Permet d'ajouter une vue a une resource
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de l'resource
 * @return
 **/
$app->route('POST /api/v1.0/resources/view', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id');
    $res= $resource_id[0];
    $where = ['user_id' => $user_id, 'resource_id' => $res];
    $cond = ["AND" => $where];
    $has_view = $db->has('views', $cond);
    save_data($app, "username", $username);
    save_data($app, "token", $token);
    save_data($app, "resource_id", $res);
    save_data($app, "user_id", $user_id);
    save_data($app, "number", 1);
    if($has_view){
        $val = $db->update('views', ['number[+]' => 1], $cond);
        if($val == 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    else{
        $val = $db->insert('views', ['user_id' => $user_id,
            'resource_id' => $res,
            'number' => 1]);
        if($val == 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    $app->send();
}, true);
/**
 * Permet d'ajouter un partage a une resource
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de l'resource
 * @return
 **/
$app->route('POST /api/v1.0/resources/share', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id');
    $res= $resource_id[0];
    $where = ['user_id' => $user_id, 'resource_id' => $res];
    $cond = ["AND" => $where];
    $has_share = $db->has('shares', $cond);
    if($has_share){
        $val = $db->update('shares', ['number[+]' => 1], $cond);
        if($val == 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    else{
        $val = $db->insert('shares', ['user_id' => $user_id,
            'resource_id' => $res,
            'number' => 1]);
        if($val == 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    $app->send();
}, true);

/**
 * Permet d'obtenir les shares
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id (optional) id ou tableau d'id des resources que l'on veut voir
 * @param user_id (optional) user_id ou tableau de user_id pour lesquels
 * on veut voir les resources
 * @param offset (optional) rang de l'resource à partir duquel on veut voir les
 * resources
 * @param limit (optional) nombre d'resources que l'on veut récupérer
 * @param arrayexpected (optional, default value = true) indique si on veux un 
 * tableau ou un élément
 * @return les votes
 **/
$app->route('GET /api/v1.0/shares', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $resource_id = $app->validate2('resource_id', null);
    $user_id = $app->validate2('user_id', null);
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $arrayexpected = $app->validate2('arrayexpected', true);
    $where = [];
       //TODO "ORDER" =>
    if(!is_null($user_id)){
        $where['user_id'] = $user_id;
    }
    if(!is_null($resource_id)){
        $where['resource_id'] = $resource_id;
    }
    $func = 'get';
    if($arrayexpected){
        $func = 'select';
    }
    $cond = ["LIMIT" => [$offset, $limit]];
    if(!empty($where)){
        $cond["AND"] = $where;
    }
    $shares = $db->$func('shares_view', '*', $cond);
    if($shares === false && $func == "select"){
        save_data($app, 'msg', "sql error $cond", true);
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    else if($shares === false && $func == "get"){
        save_data($app, null, ["id" => 0]);
    }else{
        save_data($app, null, $shares);
    }
    $app->send();
}, true);
/**
 * Permet d'obtenir les favoris
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id (optional) id ou tableau d'id des resources que l'on veut voir
 * @param user_id (optional) user_id ou tableau de user_id pour lesquels
 * on veut voir les resources
 * @param offset (optional) rang de l'resource à partir duquel on veut voir les
 * resources
 * @param limit (optional) nombre d'resources que l'on veut récupérer
 * @param arrayexpected (optional, default value = true) indique si on veux un 
 * tableau ou un élément
 * @return les votes
 **/
$app->route('GET /api/v1.0/favorites', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $resource_id = $app->validate2('resource_id', null);
    $user_id = $app->validate2('user_id', null);
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $arrayexpected = $app->validate2('arrayexpected', true);
    $where = [];
       //TODO "ORDER" =>
    if(!is_null($user_id)){
        $where['user_id'] = $user_id;
    }
    if(!is_null($resource_id)){
        $where['resource_id'] = $resource_id;
    }
    $func = 'get';
    if($arrayexpected){
        $func = 'select';
    }
    $cond = ["LIMIT" => [$offset, $limit]];
    if(!empty($where)){
        $cond["AND"] = $where;
    }
    $favorites = $db->$func('favorites_view', '*', $cond);
    if($favorites === false && $func == "select"){
        save_data($app, 'msg', "sql error $cond", true);
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    else if($favorites === false && $func == "get"){
        save_data($app, null, ["id" => 0]);
    }else{
        save_data($app, null, $favorites);
    }
    $app->send();
}, true);

/**
 * Permet d'ajouter une resource favorite
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de l'resource
 * @return
 **/
$app->route('POST /api/v1.0/favorites', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id', null);
    $resource_id = $resource_id[0];
    $cond = ["AND" => [
        'user_id' => $user_id,
        'resource_id' => $resource_id]
    ];
    $has_fav = $db->has('favorites', $cond);
    if($has_fav){
        $app->send('BAD_PARAM');
    }
    $value = $db->insert('favorites', ['user_id' => $user_id,
        'resource_id' => $resource_id]);
    if(!$value){
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    save_data($app, "username", $username);
    save_data($app, "token", $token);
    save_data($app, "user_id", $user_id);
    save_data($app, "resource_id", $resource_id);
    save_data($app, "id", $value);
    $app->send();
}, true);
/**
 * Permet de supprimer une resource favorite
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de l'resource
 * @return
 **/
$app->route('DELETE /api/v1.0/favorites', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id');
    $where = [
        'user_id' => $user_id,
        'resource_id' => $resource_id
    ];
    $cond = ["AND" => $where];
    $db->delete('favorites', $cond);
    save_data($app, "username", $username);
    save_data($app, "token", $token);
    save_data($app, "user_id", $user_id);
    save_data($app, "resource_id", $resource_id);
    save_data($app, "id", 0);
    $app->send();
}, true);

/**
 * Permet d'obtenir les reports une resource
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de la resource
 * @param comments un commentaire texte sur le motif du report
 * @param reason un identifiant (int) du type de report.
 * @return
 **/
$app->route('GET /api/v1.0/reports', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $resource_id = $app->validate2('resource_id', null);
    $user_id = $app->validate2('user_id', null);
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $arrayexpected = $app->validate2('arrayexpected', true);
    $where = [];
       //TODO "ORDER" =>
    if(!is_null($user_id)){
        $where['user_id'] = $user_id;
    }
    if(!is_null($resource_id)){
        $where['resource_id'] = $resource_id;
    }
    $func = 'get';
    if($arrayexpected){
        $func = 'select';
    }
    $cond = ["LIMIT" => [$offset, $limit]];
    if(!empty($where)){
        $cond["AND"] = $where;
    }
    $reports = $db->$func('reports', '*', $cond);
    if($reports === false && $func == "select"){
        save_data($app, 'msg', "sql error $cond", true);
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    else if($reports === false && $func == "get"){
        save_data($app, null, ["id" => 0]);
    }else{
        save_data($app, null, $reports);
    }
    $app->send();

}, true);
/**
 * Permet de report une resource
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de l'resource
 * @param comments un commentaire texte sur le motif du report
 * @param reason un identifiant (int) du type de report.
 * @return
 **/
$app->route('POST /api/v1.0/reports', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id');
    $resource_id = $resource_id[0];
    $comments = $app->validate2('comments');
    $reason = $app->validate2('reason');
    $cond = ["AND" => [
        'user_id' => $user_id,
        'resource_id' => $resource_id]
    ];
    $has_fav = $db->has('reports', $cond);
    $value = 0;
    if(!$has_fav){
        $value = $db->insert('reports', ['user_id' => $user_id,
            'resource_id' => $resource_id,
            'comments' => $comments,
            'reason' => $reason]);
    }
    else{
        $db->update('reports',
            ['comments' => $comments,
            'reason' => $reason], $cond);
    }
    save_data($app, "username", $username);
    save_data($app, "token", $token);
    save_data($app, "user_id", $user_id);
    save_data($app, "resource_id", $resource_id);
    save_data($app, "comments", $comments);
    save_data($app, "reason", $reason);
    save_data($app, "id", $value);
    $app->send();
}, true);

/**
 * Permet de suppr un report
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de l'resource
 * @return
 **/
$app->route('DELETE /api/v1.0/reports', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id');
    $cond = ["AND" => [
        'user_id' => $user_id,
        'resource_id' => $resource_id]
    ];
    $db->delete('reports', $cond);
    $app->send();
}, true);

/**
 * Permet de sélectionner des commentaires.
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param user_id (optional) user_id ou tableau de user_id pour lesquels
 * on veut voir les resources
 * @param resource_id (optional) id ou tableau d'id des resources que l'on veut voir
 * @param offset (optional) rang de l'resource à partir duquel on veut voir les
 * resources
 * @param limit (optional) nombre d'resources que l'on veut récupérer
 * @param arrayexpected (optional, default value = true) indique si on veux un 
 * tableau ou un élément
 * @return les commentaires
 **/
$app->route('GET /api/v1.0/comments', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $user_id = $app->validate2('user_id', null);
    $resource_id = $app->validate2('resource_id', null);
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $arrayexpected = $app->validate2('arrayexpected', true);
    $where = [];
       //TODO "ORDER" =>
    if(!is_null($user_id)){
        $where['user_id'] = $user_id;
    }
    if(!is_null($resource_id)){
        $where['resource_id'] = $resource_id;
    }
    $func = 'get';
    if($arrayexpected){
        $func = 'select';
    }
    $cond = ["LIMIT" => [$offset, $limit]];
    if(!empty($where)){
        $cond["AND"] = $where;
    }
    $comments = $db->$func('comments_hiscores', '*', $cond);
    if($comments === false && $func == "select"){
        save_data($app, 'msg', "sql error $cond", true);
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    else if($comments === false && $func == "get"){
        save_data($app, null, ["id" => null]);
    }else{
        save_data($app, null, $comments);
    }
    $app->send();
}, true);
/**
 * Permet d'ajouter ou de mettre à jour un commentaire sur une resource
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de la resource
 * @param comments commentaire à sauvegarder
 * @return
 **/
$app->route('POST /api/v1.0/comments', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id');
    $comments = $app->validate2('comments');
    $id = $app->validate2('id', null);
    $cond = ["AND" => [
        'user_id' => $user_id,
        'resource_id' => $resource_id[0]]
    ];
    $has_fav = $db->has('comments', $cond);
    if(!$has_fav){
        $id = $db->insert('comments', ['user_id' => $user_id,
            'resource_id' => $resource_id[0],
            'text' => $comments]);
        if($id == 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    else{
        $val = $db->update('comments', ['text' => $comments,
            'updated[+]' => 1], $cond);
        if($val == 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    save_data($app, "username", $username);
    save_data($app, "token", $token);
    save_data($app, "user_id", $user_id);
    save_data($app, "id", $id);
    save_data($app, "comments", $comments);
    save_data($app, "resource_id", $resource_id[0]);
    $app->send();
}, true);

/**
 * Permet de suppr un commentaire
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param resource_id id de la resource
 * @return
 **/
$app->route('DELETE /api/v1.0/comments', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $resource_id = $app->validate2('resource_id');
    $id = $app->validate2('id', null);
    $cond = ["AND" => [
        'user_id' => $user_id,
        'resource_id' => $resource_id]
    ];
    $db->delete('comments', $cond);
    save_data($app, "id", null);
    $app->send();
}, true);

/**
 * Permet de voter un commentaire
 * @param username nécessaire pour le login
 * @param token nécessaire pour le login
 * @param comments_id id du commentaire
 * @param note note du commentaire
 * @return
 **/
$app->route('POST /vote/comments', function($route){
    $app = $route->app;
    $db = $app->get('db');
    list($username, $token) = $app->islogged();
    $user_id = get_user_id($db, $username);
    $comments_id = $app->validate2('comments_id');
    $note = $app->validate2('note');
    $cond = ["AND" => [
        'user_id' => $user_id,
        'comments_id' => $comments_id[0]]
    ];
    $has_fav = $db->count('comments_vote', $cond);
    if($has_fav === 0){
        $res = $db->insert('comments_vote', ['user_id' => $user_id,
            "comments_id" => $comments_id[0],
            'note' => $note]);
        if($res == 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    else{
        $res = $db->update('comments_vote', ['note' => $note], $cond);
        if($res === false || $res === 0){
            error_log(print_r($db->error(),true));
            $app->send('DB');
        }
    }
    $app->send();
}, true);

/**
 * Permet de récupérer les votes pour un commentaire
 * @param username nécessaire pour le login
 * @param token nécessaire pour le lo
 * @param comments_id (optional) id du commentaire (num or array of num)
 * @param user_id (optional) user_id ou tableau de user_id pour lesquels
 * on veut voir les resources
 * @param offset (optional) rang de l'resource à partir duquel on veut voir les
 * resources
 * @param limit (optional) nombre d'resources que l'on veut récupérer
 * @param arrayexpected (optional, default value = true) indique si on veux un 
 * tableau ou un élément
 * @return
 **/
$app->route('GET /api/v1.0/comments/votes', function($route){
    $app = $route->app;
    $db = $app->get('db');
    $app->islogged();
    $user_id = $app->validate2('user_id', null);
    $comments_id = $app->validate2('comments_id', null);
    $arrayexpected = $app->validate2('arrayexpected', true);
    $offset = $app->validate2('offset', 0);
    $limit = $app->validate2('limit', 50);
    $limit = min($limit, 50);
    $cond = ["LIMIT" => [$offset, $limit]];
    $where = [];
    if(!is_null($user_id)){
        $where['user_id'] = $user_id;
    }
    if(!is_null($comments_id)){
        $where['comments_id'] = $comments_id;
    }
    if(!empty($where)){
        $cond = ["AND" => $where];
    }
    $func = 'get';
    if($arrayexpected){
        $func = 'select';
    }
    $resources = $db->$func('comments_vote', '*', $cond);
    if($resources === false && $func == "select"){
        save_data($app, 'msg', "sql error $cond", true);
        error_log(print_r($db->error(),true));
        $app->send('DB');
    }
    else if($resources === false && $func == "get"){
        save_data($app, null, ["id" => 0]);
    }else{
        save_data($app, null, $resources);
    }
    $app->send();
}, true);


$app->map('notFound', function(){
    global $app;
    $app->json(['msg' => 'url not found',
        'status' => Config::$ERROR['UNKNOWN']]);
});
/*
function delete_resource($app){
    $db = $app->get('db');
    $resource_id = get_answer($app, 'resource_id');
    $nb_resource_id = count($resource_id);
    $user_id = get_answer($app, 'my_user_id');
    $paths = $db->select("resources", "path", ["id" => $resource_id]);
    $cond = ["AND" => ['id' => $resource_id, 'user_id' => $user_id]];
    if (!file_exists($app->get('BASE_DIR') . '/resources/.trash')) {
        mkdir($app->get('BASE_DIR') . '/resources/.trash', 0777, true);
    }
    $nb_row = $db->count('resources', $cond);
    if($nb_row == $nb_resource_id){
        $nb_row = $db->delete('resources', $cond);
        if($nb_row == $nb_resource_id){
            for($i=0; $i < $nb_resource_id;$i++){
                rename($app->get('BASE_DIR') . $resource_id[$i],
                    $app->get('BASE_DIR') . '/resources/.trash/'.basename($resource_id[$i]));
            }
        }
        else{
            $app->send('DB');
        }
    }else{
        $app->send('DB');
    }
}

function insert_resource($app){
    $db = $app->get('db');
    $category = get_answer($app, 'category');
    $username = get_answer($app, 'my_username');
    $user_id = get_answer($app, 'my_user_id');
    $resource = get_answer($app, 'resource');
    $resource_description = get_answer($app, 'resource_description');
    $resource_name = get_answer($app, 'resource_name');
    $resource_ext = get_answer($app, 'resource_ext');
    $resource_mime = get_answer($app, 'resource_mime');
    $cat_name = get_category_name($db, $category);
    $cat_dir = $app->get('BASE_DIR') . '/resources/'.$cat_name;
    if (!file_exists($cat_dir)) {
        mkdir($cat_dir, 0777, true);
    }
    $username_dir = "$cat_dir/$username";
    if (!file_exists($username_dir)) {
        mkdir($username_dir, 0777, true);
    }
    $www_filename = "/resources/$cat_name/$username/".time().".$resource_ext";
    $new_filename = $app->get('BASE_DIR') . $www_filename;
    if(rename($resource, $new_filename)){
        $resource_id = $db->insert('resources',
            ['path' => $www_filename,
            'description' => $resource_description,
            'name' => $resource_name,
            'category' => $category,
            'mime' => $resource_mime,
            'user_id' => $user_id]);
        save_data($app, 'resource_id', $resource_id);
        return True;
    }
    return False;
}*/
function create_token($app, $username, &$token, &$time_token){
    $db = $app->get('db');
    $token = uniqid(rand(), true);
    $time_token = time();
    $cond = [
        "username" => $username,
    ];
    $correct = $db->has('tokens', $cond);
    if($correct){
        $db->update('tokens',
                ['token' => $token,
                'time_token' => $time_token],
                $cond);
    }
    else{
        $db->insert('tokens',
                ['token' => $token,
                'time_token' => $time_token,
                'username' => $username]);
    }
}

function delete_token($app, $username){
    $db = $app->get('db');
    $cond = ["username" => $username];
    $correct = $db->has('tokens', $cond);
    if($correct){
        $db->delete('tokens', $cond);
    }
}
function login($app){
    $db = $app->get('db');
    list($username, $password) = $app->canlogin();
    $user = $db->get('users', "*", [
            "AND" => ["username" => $username, "password" => $password]
    ]);
    if($user === false){
        save_data($app, 'msg', 'login unauthorized', true);
        $app->send('LOGIN');
    }
    $user = $db->get('users_hiscores', "*", [
        "AND" => ["username" => $username]
    ]);
    create_token($app, $username, $token, $time_token);
    $user['token'] = $token;
    $user['id'] = $user['user_id'];
    save_data($app, null, $user);
    $app->send();
}

function logout($app, $username, $token){
    $db = $app->get('db');
    $correct = $db->has('tokens', [
            "AND" => [
                "username" => $username,
                "token" => $token
            ]
    ]);
    if($correct){
        delete_token($app, $username);
        $app->json(['status' => Config::$ERROR['NONE'],
            'username' => $username,
            'msg' => 'logged out']);
    }
    else{
        $app->json(['username' => $username,
            'status' => Config::$ERROR['ALREADY_LOGOUT'],
            'msg' => 'already logged out']);
    }
}

$app->map('send', function($status = 'NONE', $status_code = 200){
    global $app;
    switch ($status) {
    case "LOGIN":
    case "LOGIN_EXPIRED":
    case "NOT_LOGGED":
    case "TOKEN":
    case "USERNAME":
    case "BAD_PARAM":
    case "RESOURCE_SELECT_BAD_PARAM":
    case "RESOURCE_INPUT":
    case "RESOURCE_NOT_FOUND":
    case "VOTE":
    case "BANNED":
        $status_code = 400;
        break;
    case "DB":
    case "RESOURCE_DELETE":
        $status_code = 406;
        break;
    case "MISSING_PARAM":
        $status_code = 424;
        break;
    case "UNKNOWN":
        $status_code = 500;
        break;
    case "NONE":
        $status_code = 200;
        break;
    default:
        break;
    }
    if(is_string($status)){
        $status = Config::err($status);
    }
    if($status_code != 200){
        save_data($app, 'status', $status);
    }
    $app->json($app->get('answer'), $status_code);
});
$app->map('validate', function($func){
    global $app;
    $db = $app->get('db');
    $request = $app->request();
    $status = Config::$ERROR['UNKNOWN'];
    $validate_func = "validate_".$func;
    if(!is_callable($validate_func)){
        save_data($app, 'msg', "validation error : $validate_func", true);
        error_log(get_answer($app, 'msg'));
        answer($app, $status, 500);
    }
    else if(!$validate_func($app, $db, $status)){
        save_data($app, 'msg', "validation failed : ", 'prepend');
        error_log(get_answer($app, 'msg'));
        answer($app, $status, 400);
    }
});
$app->map('validate2', function($func, $dft=null){
    global $app;
    $db = $app->get('db');
    $validated = $app->get('validated');
    $status = Config::$ERROR['UNKNOWN'];
    $validate_func = "validate_filter_".$func;
    $returnvalue = null;
    if(array_key_exists($func, $validated)){
        $returnvalue = $validated[$func];
    }
    else if(!is_callable($validate_func)){
        $msg = "validation error : $validate_func";
        save_data($app, 'msg', $msg, true);
        error_log($msg);
        $app->send('UNKNOWN');
    }
    else{
        $returnvalue = null;
        if(func_num_args()>1){
            $returnvalue = $validate_func($app, $dft);
        }else{
            $returnvalue = $validate_func($app);
        }
        $validated[$func] = $returnvalue;
        $app->set('validated', $validated);
    }
    return $returnvalue;
});

$app->map('is_username_available', function(){
    global $app;
    $db = $app->get('db');
    $username = $app->validate2('username');
    $correct = $db->has('users', ["username" => $username]);
    if($correct){
        $msg = 'username ' . $username . ' already taken';
        save_data($app, 'msg', $msg, true);
        $app->send('USERNAME');
    }
    return true;
});
$app->map('is_banned', function(){
    global $app;
    $db = $app->get('db');
    $username = $app->validate2('username');
    $user_id = get_user_id($db, $username);
    $uuid = $app->validate2('uuid');
    $id_banned = $db->count('banlist', [
        "user_id" => $user_id
    ]);
    $uuid_banned = $db->count('banlist', [
        "uuid" => $uuid
    ]);
    if($uuid_banned >0 || $id_banned > 0){
        $msg = "user $username and/or uuid $uuid is banned";
        save_data($app, 'msg', $msg, true);
        $app->send('BANNED');
    }
    return false;
});
$app->map('canregister', function(){
    global $app;
    $db = $app->get('db');
    $username = $app->validate2('username');
    $pass1 = $app->validate2('pass1');
    $uuid = $app->validate2('uuid');
    $app->is_username_available();
    $app->is_banned();
    return [$username, $pass1, $uuid];
});
$app->map('canlogin', function(){
    global $app;
    $db = $app->get('db');
    $username = $app->validate2('username');
    $pass1 = $app->validate2('pass1');
    $app->is_banned();
    return [$username, $pass1];
});
$app->map('savefile', function($file, $destfolder, $newfilename){
    global $app;
    $db = $app->get('db');
    $mime = '';
    $wwwpath = '';
    try{
        $im = new Imagick($file);
        $mime = extractMime($file);
        $ext = getImageExtension($mime);
        if($ext === False){
            $status = Config::$ERROR['RESOURCE_INPUT'];
            save_data($app, 'msg', "mime $mime not supported", true);
            $app->send('RESOURCE_INPUT');
        }
        $im->setImageFormat('jpg');
        $wwwpath = "/$destfolder/$newfilename.jpg";
        $tmppath = $app->get('BASE_DIR').$wwwpath;
        $size = filesize($file);
        if($size > 512*1024){
            $im->setCompression(50);
        }
        $im->writeImage($tmppath);
        $mime = 'image/jpeg';
    }catch(ImagickException $e){
        $msg = "Error in file $name conversion";
        error_log($msg . " => ". $e->getMessage());
        save_data($app, 'msg', $msg, true);
        $app->send('RESOURCE_INPUT');
    }
    return [$wwwpath, $mime];
});
$app->map('islogged', function(){
    global $app;
    $db = $app->get('db');
    $username = $app->validate2('username');
    $token = $app->validate2('token');
    $cond = [
        "AND" => [
            "username" => $username,
            "token" => $token,
        ]
    ];
    $time_token = time();
    $last_time = $db->get('tokens', 'time_token', $cond);
    if($last_time === false){
        $msg = "token error, user is not logged";
        save_data($app, 'msg', $msg, true);
        $app->send('NOT_LOGGED');
    }
    else if($time_token > $last_time + Config::SESSION_TIMEOUT){
        $msg = "token freshness error";
        save_data($app, 'msg', $msg, true);
        $app->send('LOGIN_EXPIRED');
    }
    $time_token = time();
    $db->update('tokens',
        ['time_token' => $time_token],
        ['username' => $username]);
    return [$username, $token];
});
$app->start();
