#!/bin/bash
NONE='\033[00m'
RED='\033[01;31m'
GREEN='\033[01;32m'
YELLOW='\033[01;33m'
PURPLE='\033[01;35m'
CYAN='\033[01;36m'
WHITE='\033[01;37m'
BOLD='\033[1m'
UNDERLINE='\033[4m'
count_check () {
    echo -ne [${YELLOW}CHECK${NONE}] Count field $2
    value=$(echo $1 | jq ".[\"$2\"] | length")
    [ $value != "$3" ] && echo -e ... [${RED}ERROR${NONE}] $2 has $value elements instead of $3 && exit 1
    echo -e ... ${GREEN}DONE${NONE}
}
simple_check () {
    echo -ne [${YELLOW}CHECK${NONE}] Check field $2
    value=$(echo $1 | jq ".[\"$2\"]")
    [ $value != "$3" ] && echo -e ... [${RED}ERROR${NONE}] $2 has the value $value instead of $3 && exit 1
    echo -e ... ${GREEN}DONE${NONE}
}
display(){
    echo $@ | jq '.'
}
get() {
    value=$(echo $1 | jq ".[\"$2\"]")
    eval "$2=$value"
}

login_output=$(curl -d username=vincent \
    -d pass1=38256fbe4e80d9ffd355409f36238ae18e62c668208c259e60ca323ab47cf55b8656e88e56593d531b250aae2c35376b387d83ade5e3e8b6c042133b97030fa4 \
    http://test.changala.fr/login)
display $login_output
simple_check "$login_output" status 0
simple_check "$login_output" username '"vincent"'
get "$login_output" token

select_resource=$(curl -d username=vincent -d token="$token" \
        -d categories=1 \
        -d offset=1 http://test.changala.fr/resources/select)
display $select_resource
simple_check "$login_output" status 0

insert_resource=$(curl -d username=vincent -d token="$token" \
        -d category=7 \
        -d name="Marvelous joke" \
        -d data="This is a joke" http://test.changala.fr/resources/insert)
display $insert_resource
get "$insert_resource" resource_id
simple_check "$insert_resource" status 0
simple_check "$insert_resource" resource_ext '"txt"'

select_resource=$(curl -d username=vincent -d token="$token" \
        -d categories=7 \
        -d resource_id=$resource_id http://test.changala.fr/resources/select)
display $select_resource
simple_check "$select_resource" status 0
count_check "$select_resource" resources 1

vote_resource=$(curl -d username=vincent -d token="$token" \
        -d note=4 \
        -d resource_id=$resource_id http://test.changala.fr/resources/vote)
display $vote_resource
simple_check "$vote_resource" status 0

select_vote_resource=$(curl -d username=vincent -d token="$token" \
        -d resource_id=$resource_id http://test.changala.fr/resources/vote/select)
display $select_vote_resource
simple_check "$select_vote_resource" status 0
simple_check "$select_vote_resource" votes '[4]'

delete_resource=$(curl -d username=vincent -d token="$token" \
        -d category=7 \
        -d resource_id=$resource_id \
         http://test.changala.fr/resources/delete)
display $delete_resource

logout_output=$(curl -d username=vincent -d token="$token" http://test.changala.fr/logout)
display $logout_output
simple_check "$login_output" status 0
simple_check "$login_output" username '"vincent"'
