var rymodels = require('./models');
var ryutils = require('./utils');
var mime = require('mime');
var child_process = require('child_process');
var gifsicle = require('gifsicle');
var im = require('imagemagick');
var fs = require("fs"); //Load the filesystem module


function validate_pass1(req, res, next) {
    req.assert('pass1', 'Required pass1').isLength({min:1});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        return next('PASSWORD');
    }
    else{
        res.tmpparams.pass1 = req.sanitize('pass1').escape();
    }
    return next();
}
function validate_name(opt) {
    return function (req, res, next) {
        if(!opt){
            req.assert('name', 'Unexpected name [4, 50]').isLength({min:4, max:50});
        }else{
            req.assert('name', 'Unexpected name [4, 50]')
                .optional()
                .isLength({min:4, max:50});
        }
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        return next('BAD_PARAM');
    }
    else{
        res.tmpparams.name = req.sanitize('name').escape();
    }
    return next();
    }
}
function validate_text(opt) {
    return function (req, res, next) {
        if(!opt){
            req.assert('text', 'Unexpected text [0, 255]').isLength({min:0, max:255});
        }else{
            req.assert('text', 'Unexpected text [0, 255]')
                .optional()
                .isLength({min:0, max:255});

        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            return next('BAD_PARAM');
        }
        else{
            res.tmpparams.text = req.sanitize('text').escape();
        }
        return next();
    }
}
function validate_username(req, res, next) {
    req.assert('username', 'Required username').notEmpty();
    req.assert('username', 'Invalid username').isAlphanumeric();
    //req.assert('username', 'Invalid username').matches(/^\w+$/);
    req.assert('username', '4 to 30 characters required').len(4, 30);
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        return next('username');
    }
    else{
        res.tmpparams.username = req.param('username');
    }
    return next();
}

function validate_reason(req, res, next) {
    req.assert('reason', 'Unexpected reason')
    .isInt({min: 0});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in reason parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.reason = req.sanitize('reason').toInt();
    return next();
}
function validate_limit(req, res, next) {
        console.log("validate_limit");
    req.assert('limit', 'Unexpected limit')
    .optional()
    .isInt({min: 0});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in limit parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.limit = parseInt(req.param('limit', 50));
    return next();
}
function validate_uuid(opt) {
    return function (req, res, next) {
        var method = (opt.method)? req[opt.method]: req.assert;
        if(opt.optional){
            method('uuid', 'Unexpected uuid')
                .optional().isUUID();
        }
        else{
            method('uuid', 'Unexpected uuid').isUUID();
        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            res.tmprep.errmsg = "error in uuid parameters";
            return next('BAD_PARAM');
        }
        res.tmpparams.uuid = (opt.method == "checkHeaders")? req.get('uuid'): req.param('uuid');
        return next();
    }
}
function validate_user_id(opt) {
    return function (req, res, next) {
        console.log("validate_user_id");
        if(!opt){
            req.assert('user_id', 'Unexpected user_id')
                .isUUID();
        }
        else{
            return next();
            req.assert('user_id', 'Unexpected user_id')
                .optional()
                .isIntArray();

        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            res.tmprep.errmsg = "error in user_id parameters";
            return next('BAD_PARAM');
        }
        res.tmpparams.user_id = req.param('user_id');
        return next();
    }
}
function validate_categories(opt) {
    return function (req, res, next) {
        console.log("validate_categories");
        if(!opt){
            req.assert('category_id', 'Unexpected categories')
                .isInt({min: 0});
        }else{
            req.assert('category_id', 'Unexpected categories')
                .optional().isIntArray();
        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            res.tmprep.errmsg = "error in categories parameters";
            return next('BAD_PARAM');
        }
        res.tmpparams.category_id = req.param('category_id');
        return next();
    }
}
function validate_type(req, res, next) {
    req.assert('type', 'Unexpected type')
        .isInt({min: 0, max: 1});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "unsupported type parameter";
        return next('BAD_PARAM');
    }
    res.tmpparams.type = req.param('type');
    return next();
}
function validate_token(req, res, next) {
    req.checkHeaders('authorization', 'Unexpected token').isLength({min:1});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in token parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.token = req.sanitizeHeaders('authorization').escape();
    return next();
}
function validate_offset(req, res, next) {
        console.log("validate_offset");
    req.assert('offset', 'Unexpected offset')
        .optional()
        .isInt({min: 0});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in offset parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.offset = parseInt(req.param('offset', 0));
    return next();
}
function validate_method(req, res, next) {
        console.log("validate_method");
    req.assert('method', 'Unexpected method')
        .optional().isIn(['hiscores', 'random']);
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in method parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.method = req.param('method', 'hiscores');
    return next();
}
function validate_resource_id(opt) {
    return function (req, res, next) {
        console.log("validate_resource_id");
        if(!opt){
            req.assert('resource_id', 'Unexpected resource_id')
                .isInt({min: 0});
        }else{
            req.assert('resource_id', 'Unexpected resource_id')
                .optional()
                .isIntArray();

        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            res.tmprep.errmsg = "error in resource_id parameters";
            return next('BAD_PARAM');
        }
        res.tmpparams.resource_id = req.param('resource_id');
        return next();
    }
}
function validate_commentsnote(req, res, next) {
    req.assert('note', 'Unexpected commentsnote')
    .isNumeric();
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in commentsnote parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.note = req.param('commentsnote');
    return next();
}
function validate_note(req, res, next) {
    req.assert('note', 'Unexpected note')
    .isInt({min: 0, max: 5});
    res.tmprep.errmapped = req.validationErrors(true);
    if(res.tmprep.errmapped){
        res.tmprep.errmsg = "error in note parameters";
        return next('BAD_PARAM');
    }
    res.tmpparams.note = req.param('note');
    return next();
}
function validate_file(opt) {
    return function (req, res, next) {
        function writefile(err){
            console.log("pre writeFile");
            if(err) {
                console.log(err);
                res.tmprep.errmsg = err;
                return next('COPY_ERROR');
            }
            if(res.tmpparams.type == 0){
                res.tmpparams.file.filename = res.tmpparams.uuid + '_' + Date.now() + ".png";
                res.tmpparams.file.path = "./data/" + res.tmpparams.file.filename;
                console.log("pre ip.resize");
                im.resize({
                    srcPath: res.tmpparams.file.tmppath,
                    dstPath: res.tmpparams.file.path,
                    format: "png",
                    width: 800,
                    quality: 0.8
                }, function(err, stdout, stderr){
                    if (err){
                        res.tmprep.errmsg = err;
                        return next('BAD_PARAM');
                    }
                    console.log("resized");
                    return next();
                });
            }else if(res.tmpparams.type == 1){
                res.tmpparams.file.filename = res.tmpparams.uuid + '_' + Date.now() + ".gif";
                res.tmpparams.file.path = "./data/" + res.tmpparams.file.filename;
                gifsicleopt = ['--optimize', '--resize-fit-width=800',
                    '-o', req.tmpparams.path, res.tmpparams.tmppath];
                function gifsiclecallback(err) {
                    if (err){
                        res.tmprep.errmsg = err;
                        return next('BAD_PARAM');
                    }
                    return next();
                };
                child_process.execFile(gifsicle, gifsicleopt, gifsiclecallback);
            }
        }
        //var size = ryutils.getFilesizeInBytes(req.file.path);
        //var _mime = mime.lookup(req.file.path);
        var mimevalidation = ["image/png", "image/gif"];
        req.assert('data', "data is note base64 encoded").isBase64();
        console.log(req.validationErrors(true));
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            res.tmprep.errmsg = "error in data parameters";
            console.log("bad param");
            return next('BAD_PARAM');
        }
        if(typeof mimevalidation[res.tmpparams.type] === 'undefined'){
            res.tmprep.errmsg = "Type " + res.tmpparams.type + "is currently not supported";
            return next('BAD_PARAM');
        }
        res.tmpparams.data = req.param('data');
        res.tmpparams.file = {};
        res.tmpparams.file.tmppath = "./tmp/tmp_" + Date.now() + ".tmp";
        fs.writeFile(res.tmpparams.file.tmppath,
                     res.tmpparams.data, 'base64',
                     writefile);
        console.log("end fs.writeFile");
        /*if(size > 20 * 1024 * 1024){
            res.tmprep.errmsg = "FileSize too high (>20Mo)";
            return next('BAD_PARAM');
        }*/
    }
}

function validate_id(opt) {
    return function (req, res, next) {
        if(!opt){
            req.assert('id', 'Unexpected id')
                .isInt({min: 0});
        }
        else{
            req.assert('id', 'Unexpected id')
                .optional()
                .isInt({min: 0});

        }
        res.tmprep.errmapped = req.validationErrors(true);
        if(res.tmprep.errmapped){
            res.tmprep.errmsg = "error in id parameters";
            return next('BAD_PARAM');
        }
        res.tmpparams.id = req.param('id');
        return next();
    }
}
exports.validate_resource_id = validate_resource_id;
exports.validate_note = validate_note;
exports.validate_pass1 = validate_pass1;
exports.validate_categories = validate_categories;
exports.validate_method = validate_method;
exports.validate_username = validate_username;
exports.validate_id = validate_id;
exports.validate_user_id = validate_user_id;
exports.validate_limit = validate_limit;
exports.validate_type = validate_type;
exports.validate_offset = validate_offset;
exports.validate_uuid = validate_uuid;
exports.validate_token = validate_token;
exports.validate_text = validate_text;
exports.validate_name = validate_name;
exports.validate_file = validate_file;
exports.validate_commentsnote = validate_commentsnote;
exports.validate_reason = validate_reason;
