var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var multer = require('multer'); // v1.0.5
var upload = multer({dest: './uploads/'}); // for parsing multipart/form-data
var rymodels = require('./models');
var rycheck = require('./validate');
var ryutils = require('./utils');
var crypto = require('crypto');
var dateFormat = require('dateformat');
var mime = require('mime');
var request = require('request');
var ionicrequest = request.defaults({
      headers: {
          'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJhZDA0ZDBjZS02NTgzLTQwNWEtODk4YS0xY2RiYjRkODgwOWUifQ.HWRsDOkINlb1cDAIc2MofaRCdabo6Scyi3z3YRHQzmg',
          'content-type': 'application/json'
      },
      baseUrl : "https://api.ionic.io/",
      json: true
});
var app = express();
// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization, uuid');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
app.use(cors());
app.use('/data', express.static(__dirname + '/data'));
app.use('/img_cat', express.static(__dirname + '/img_cat'));
app.use('/well-known', express.static(__dirname + '/well-known'));
app.use(express.static('.well-known'));
app.use(bodyParser.json({limit:'5mb'}));
app.use(expressValidator({
    customSanitizers: {
        defaulted: function(value, dft) {
            var newValue = dft;
            if(typeof(value) !== 'undefined'){
                newValue = value;
            }
            return newValue;
        }
    },
    customValidators: {
        isIntArray: function(value) {
            var i = 0;
            function isNormalInteger(str) {
                var n = ~~Number(str);
                return String(n) === str && n >= 0;
            }
            if(!Array.isArray(value)){
                return false;
            }
            for(i = 0; i < value.length;i++){
                if(!isNormalInteger(value[i])){
                    return false;
                }

            }
            return Array.isArray(value);
        },
        isArray: function(value) {
            return Array.isArray(value);
        },
        gte: function(param, num) {
            return param >= num;
        },
    }
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.all('/api/*', function(req, res, next){
    res.tmprep = {}; // contains the built response to send
    res.tmpparams = {}; //contains parameters validated and sanitized
    next();
});
app.all('/api/v1.1/p/*', rycheck.validate_uuid({method:'checkHeaders'}));
//app.all('/api/v1.1/p/*', rycheck.validate_token);
app.all('/api/v1.1/p/*', islogged);
/* Hiscores */
app.route('/api/v1.1/p/hiscores')
    .get(rycheck.validate_user_id('optional'))
    .get(rycheck.validate_limit)
    .get(rycheck.validate_offset)
    .get(get_hiscores);
/* Resources */
app.route('/api/v1.1/p/resources*')
    .get(rycheck.validate_resource_id("optional"))
    .get(rycheck.validate_user_id('optional'))
    .get(rycheck.validate_limit)
    .get(rycheck.validate_offset);
app.route('/api/v1.1/p/resources/+')
    .post(rycheck.validate_resource_id());
app.route('/api/v1.1/p/resources')
    .get(rycheck.validate_method)
    .get(rycheck.validate_categories("optional"))
    .get(rycheck.validate_type)
    .get(get_resources)
    .post(rycheck.validate_id("optional"))
    //.post(rycheck.validate_user_id())
    .post(rycheck.validate_type)
    .post(rycheck.validate_text("optional"))
    .post(rycheck.validate_name("optional"))
    .post(rycheck.validate_categories())
    .post(rycheck.validate_file())
    //.post(upload.single('file'))
    .post(set_resources);
app.route('/api/v1.1/p/resources/views')
    .get(get_resources_views)
    .post(set_resources_views);
app.route('/api/v1.1/p/resources/votes')
    .get(get_resources_votes)
    .post(rycheck.validate_note)
    .post(set_resources_votes);
app.route('/api/v1.1/p/resources/shares')
    .get(get_resources_shares)
    .post(set_resources_shares);
app.route('/api/v1.1/p/resources/favorites')
    .get(get_resources_favorites)
    .post(set_resources_favorites)
    .delete(del_resources_favorites);
app.route('/api/v1.1/p/resources/reports')
    .get(get_resources_reports)
    .post(rycheck.validate_text)
    .post(rycheck.validate_reason)
    .post(set_resources_reports)
    .delete(del_resources_reports);
app.route('/api/v1.1/p/resources/comments')
    .get(get_resources_comments)
    .post(rycheck.validate_text)
    .post(set_resources_comments)
    .delete(del_resources_comments);
app.route('/api/v1.1/p/comments/votes')
    .get(get_comments_votes)
    .post(rycheck.validate_commentsnote)
    .post(set_comments_votes)
/*app.route('/api/v1.1/register')
    .post(rycheck.validate_username)
    .post(rycheck.validate_pass1)
    .post(rycheck.validate_uuid)
    .post(register)
    .post(login)
    .post(is_banned)
    .post(create_token);*/
app.route('/api/v1.1/login')
    //.post(rycheck.validate_username)
    //.post(rycheck.validate_pass1)
    .all(rycheck.validate_uuid({method:'checkHeaders'}))
    .post(login)
    .post(is_banned)
    .post(create_token);
app.route('/api/config')
    .get(get_config);
app.route('/api/v1.1/lang')
    .get(get_lang);
app.route('/api/v1.1/categories')
    .get(get_categories);
app.all('/api/*', function(req, res, next){
    res.json(res.tmprep);
});
app.use(errorHandler);
function errorHandler(err, req, res, next) {
    if (res.headersSent) {
        return next(err);
    }
    res.status(500);
    if(!res.tmprep){
        res.tmprep = {};

    }
    res.tmprep.err = true;
    res.tmprep.errlabel = err;
    res.json(res.tmprep);
}
function create_token(req, res, next) {
    crypto.randomBytes(128, function(err, buffer) {
          var newtoken = buffer.toString('hex');
          var time_token = Math.round(new Date().getTime()/1000);
          rymodels.tokens.findOrCreate({
              where: {
                  user_id: res.tmpparams.uuid
              },
              defaults:{
                  token: newtoken,
                  time_token: time_token
              }
          }).spread(function(tok, created){
              res.tmprep.token = newtoken;
              res.tmprep.time_token = time_token;
              if(!created){
                  tok.token = newtoken;
                  tok.time_token = time_token;
                  tok.save();
              }
              return next();
          }).error(ryutils.errhook(req, res, next));
    });
}

function register(req, res, next) {
    rymodels.users.findOrCreate({where: {
        username : res.tmpparams.username,
        uuid : res.tmpparams.uuid
    },
    defaults: {
        password : res.tmpparams.pass1
    }
    }).spread(function(user, created){
        if(!created){
            return next('ALREADY_EXIST');
        }
        return next();
    }).error(ryutils.errhook(req, res, next));
}

function login(req, res, next) {
    ionicrequest.get("users/" + res.tmpparams.uuid, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            /* user has been found */
            rymodels.users.findOrCreate({where: {
                uuid : res.tmpparams.uuid
            }
            }).spread(function(user, created){
                if(!created){
                    //return next('ALREADY_EXIST');
                }
                return next();
            }).error(ryutils.errhook(req, res, next));
        }
        else{
            return next("NOT_LOGGED");
        }
    });
    /*rymodels.users.findOne({where:{
        username : res.tmpparams.username,
        password : crypto.createHash('sha256').update(res.tmpparams.pass1).digest('base64')
        }}).then(function(user){
            if(!user){
                return next("NOT_LOGGED");
            }
            return rymodels.hiscores.findOne({where:{
                user_id: user.uuid,
            }}).then(function(user2){
                res.tmprep.data = user2;
                res.tmprep.uuid = user.uuid;
                res.tmpparams.uuid = user.uuid;
                return next();
            }).error(ryutils.errhook(req, res, next));

        }).error(ryutils.errhook(req, res, next));
       */
}
function is_banned(req, res, next) {
    rymodels.banlist.count({ where : {$or :[
        { user_id: res.tmpparams.uuid },
        { uuid: res.tmpparams.uuid }
    ]}
    }).then(function(count){
        if(count > 0){
            return next("BANNED");
        }
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function islogged(req, res, next) {
    res.tmpparams.uuid = req.get('uuid');
    ionicrequest.get("users/" + res.tmpparams.uuid,
                     function (error, response, body) {
        console.log(body);
        if (!error && response.statusCode == 200) {
            return next();
        }
        else{
            return next('NOT_LOGGED');
        }
    });
    /*var test = rymodels.tokens.findOne({
        where: {
            user_id: res.tmpparams.uuid,
            token: res.tmpparams.token
        }
    }).then(function (token){
        if(token != null){
            var time = Math.floor(Date.now() / 1000);
            if(time > token.time_token + 15 * 60){
                return next('LOGIN_EXPIRED');
            }else{
                token.time_token = time;
                res.tmprep.time_token = time;
                token.save().then(function(){
                    return next();
                }).error(ryutils.errhook(req, res, next));
            }
        }else{
            return next('NOT_LOGGED');
        }
    }).error(ryutils.errhook(req, res, next));*/
}
function get_config(req, res, next) {
    var query = rymodels.config;
    query.findAll({raw: true}).then(function (config){
        res.tmprep.data = config;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_lang(req, res, next) {
    var query = rymodels.lang;
    query.findAll({raw: true}).then(function (lang){
        res.tmprep.data = lang;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_categories(req, res, next) {
    var query = rymodels.categories.scope('augmented');
    query.findAll({raw:true,
        subQuery:false,
    }).then(function (cats){
        res.tmprep.data = cats;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function set_resources(req, res, next) {
    /*var output = './data/' + res.tmpparams.file.filename2;
    var source = fs.createReadStream(res.tmpparams.file.path2);
    var dest = fs.createWriteStream(output);*/
    var _where = {};
    var _defaults = {};
    _defaults.path = res.tmpparams.file.path.substring(6);//remove dot character
    _defaults.type = res.tmpparams.type;
    _defaults.category_id = res.tmpparams.category_id;
    _defaults.user_id = res.tmpparams.uuid;
    _defaults.name = res.tmpparams.name;
    _defaults.description = res.tmpparams.text;
    _where.id = ryutils.setWhereClause2(res.tmpparams.id);
    console.log(_where);
    console.log(_defaults);
    rymodels.resources.findOrCreate({
        where: _where,
        defaults: _defaults
    }).spread(function(res, created){
        console.log("done created:"+ created);
        return next();
    }).error(ryutils.errhook(req, res, next));
    /*fs.writeFile(output, res.tmpparams.file.data, 'base64', function(err) {
        if(err) {
            console.log(err);
            res.tmprep.errmsg = err;
            return next('COPY_ERROR');
        }
});
*/
    /*source.pipe(dest);
    source.on('end', function() {
        var _where = {};
        var _defaults = {};
        _defaults.path = output;
        _defaults.name = res.tmpparams.name;
        _defaults.type = res.tmpparams.type;
        _defaults.mime = res.tmpparams.file.mime;
        ryutils.setWhereClause(_defaults, 'description', res.tmpparams.text);
        _where.user_id = res.tmpparams.user_id;
        ryutils.setWhereClause(_where, 'id', res.tmpparams.id);
        rymodels.resources.findOrCreate({
            where: _where,
            defaults: _defaults
        }).spread(function(res, created){

        }).error(ryutils.errhook(req, res, next));
    });
    source.on('error', function(err) {
        res.tmprep.errmsg = err;
        return next('COPY_ERROR');
    });*/

}
function get_hiscores(req, res, next) {
    var options = {
        where: {
        },
        subQuery:false,
        raw: true,
        limit: res.tmpparams.limit,
        offset: res.tmpparams.offset
    };
    ryutils.setWhereClause(options.where, 'user_id', res.tmpparams.user_id);
    //options.order = [ [rymodels.sequelize.col('total') , 'DESC']];
    var query = rymodels.hiscores.scope('augmented');
    query.findAll(options).then(function (ress){
        res.tmprep.data = ress;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_resources(req, res, next) {
    var options = {
        where: {
            type: res.tmpparams.type
        },
        subQuery:false,
        raw: true,
        limit: res.tmpparams.limit,
        offset: res.tmpparams.offset
    };
    ryutils.setWhereClause(options.where, 'user_id', res.tmpparams.user_id);
    ryutils.setWhereClause(options.where, 'category_id', res.tmpparams.category_id);
    if(res.tmpparams.method == 'random'){
        options.order = [[ rymodels.sequelize.fn( 'random' )]];
    }
    else{
        options.order = [ [rymodels.sequelize.col('avg') , 'DESC']];
    }
    var query = rymodels.resources.scope('augmented');
    query.findAll(options).then(function (ress){
        res.tmprep.data = ress;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_resources_views(req, res, next) {
    var query = rymodels.resources_views;
    query.findAll({where: {
        user_id: res.tmpparams.user_id,
        resource_id: res.tmpparams.resource_id
        },
        raw: true,
        subQuery:false,
        limit: res.tmpparams.limit,
        offset: res.tmpparams.offset
    }).then(function (views){
        res.tmprep.data = views;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function set_resources_views(req, res, next) {
    var query = rymodels.resources_views;
    query.findOrCreate({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
        },
        defaults:{
            number:1
        }
    }).spread(function (view, created){
        if(!created){
            view.number = view.number + 1;
            view.save();
        }
        res.tmprep.data = view;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_resources_shares(req, res, next) {
    var query = rymodels.resources_shares;
    var options = {
        where: {},
        raw: true,
        subQuery:false,
        limit: res.tmpparams.limit,
        offset: res.tmpparams.offset
    };
    ryutils.setWhereClause(options.where, 'user_id', res.tmpparams.user_id);
    ryutils.setWhereClause(options.where, 'resource_id', res.tmpparams.resource_id);
    query.findAll(options).then(function (shares){
        res.tmprep.data = shares;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function set_resources_shares(req, res, next) {
    var query = rymodels.resources_shares;
    query.findOrCreate({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
        },
        defaults:{
            number: 1
        }
    }).spread(function (share, created){
        if(!created){
            share.number = share.number + 1;
            share.save();
        }
        res.tmprep.data = share;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_resources_favorites(req, res, next) {
    var query = rymodels.favorites.scope('augmented');
    var options = {
        where: {},
        raw: true,
        subQuery:false,
        limit: res.tmpparams.limit,
        offset: res.tmpparams.offset
    };
    ryutils.setWhereClause(options.where, 'user_id', res.tmpparams.user_id);
    ryutils.setWhereClause(options.where, 'resource_id', res.tmpparams.resource_id);
    query.findAll(options).then(function (favorites){
        res.tmprep.data = favorites;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function set_resources_favorites(req, res, next) {
    var query = rymodels.favorites;
    query.findOrCreate({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
        },
        raw: true
    }).spread(function (favorite, created){
        res.tmprep.data = favorite;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function del_resources_favorites(req, res, next) {
    var query = rymodels.favorites;
    query.findOne({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
        }
    }).then(function (favorite){
        if(!favorite){
            return next('FAVORITE_NOT_EXISTING');
        }
        favorite.destroy();
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_resources_comments(req, res, next) {
    var query = rymodels.comments.scope('augmented');
    query.findAll({where: {
        user_id: res.tmpparams.user_id,
        resource_id: res.tmpparams.resource_id
        },
        raw: true,
        subQuery:false,
        limit: res.tmpparams.limit,
        offset: res.tmpparams.offset
    }).then(function (comments){
        res.tmprep.data = comments;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function set_resources_comments(req, res, next) {
    var query = rymodels.comments;
    query.findOrCreate({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
    },
    defaults: {
        text: res.tmpparams.text,
        updated: 1
    }
    }).spread(function (comment, created){
        if(!created){
            if(res.tmpparams.text){
                comment.text = res.tmpparams.text;
            }
            comment.updated = comment.updated + 1;
            comment.save();
        }
        res.tmprep.data = comment;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function del_resources_comments(req, res, next) {
    var query = rymodels.comments;
    query.findOne({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
    }
    }).then(function (comment){
        if(!comment){
            return next('COMMENT_NOT_EXISTING');
        }
        comment.destroy();
        return next();
    });
}
function get_resources_reports(req, res, next) {
    var query = rymodels.resources_reports;
    query.findAll({where: {
        user_id: res.tmpparams.user_id,
        resource_id: res.tmpparams.resource_id
        },
        limit: res.tmpparams.limit,
        subQuery:false,
        raw:true,
        offset: res.tmpparams.offset
    }).then(function (reports){
        res.tmprep.data = reports;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function set_resources_reports(req, res, next) {
    var query = rymodels.resources_reports;
    query.findOrCreate({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
    },
    defaults: {
        text: res.tmpparams.text,
        reason: res.tmpparams.reason
    }
    }).spread(function (report, created){
        if(!created){
            if(res.tmpparams.comments){
                report.text = res.tmpparams.text;
            }
            if(res.tmpparams.reason){
                report.reason = res.tmpparams.reason;
            }
            report.save();
        }
        res.tmprep.data = report;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function del_resources_reports(req, res, next) {
    var query = rymodels.resources_reports;
    query.findOne({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
        }
    }).then(function (report){
        if(!report){
            return next('REPORT_NOT_EXISTING');
        }
        report.destroy();
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_resources_votes(req, res, next) {
    var query = rymodels.resources_votes;
    query.findAll({where: {
        user_id: res.tmpparams.user_id,
        resource_id: res.tmpparams.resource_id
        },
        raw: true,
        subQuery:false,
        limit: res.tmpparams.limit,
        offset: res.tmpparams.offset
    }).then(function (votes){
        res.tmprep.data = votes;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function set_resources_votes(req, res, next) {
    var query = rymodels.resources_votes;
    query.findOrCreate({where: {
        user_id: res.tmpparams.uuid,
        resource_id: res.tmpparams.resource_id
    },
    defaults: {
        note: res.tmpparams.note
    }
    }).spread(function (vote, created){
        if(!created){
            vote.note = res.tmpparams.note;
            vote.save();
        }
        res.tmprep.data = vote;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function get_comments_votes(req, res, next) {
    var query = rymodels.comments_votes;
    query.findAll({where: {
        user_id: res.tmpparams.user_id,
        comments_id: res.tmpparams.comments_id
        },
        raw: true,
        subQuery:false,
        limit: res.tmpparams.limit,
        offset: res.tmpparams.offset
    }).then(function (votes){
        res.tmprep.data = votes;
        return next();
    }).error(ryutils.errhook(req, res, next));
}
function set_comments_votes(req, res, next) {
    var query = rymodels.comments_votes;
    var where = {};
    ryutils.setWhereClause(where, 'user_id', res.tmpparams.uuid);
    ryutils.setWhereClause(where, 'comment_id', res.tmpparams.comment_id);
    query.findOrCreate({where: where,
        defaults: {
            note: res.tmpparams.note
        },
    }).spread(function (vote, created){
        if(!created){
            vote.note = res.tmpparams.note;
            vote.save();
        }
        res.tmprep.data = vote;
        return next();
    }).error(ryutils.errhook(req, res, next));
}

function init(){
    var config = rymodels.config;
    var categories = rymodels.categories;
   return config.bulkCreate([
        { name: 'version', value: '1.1'},
        { name: 'api_addr', value: 'https://ratemy2.changala.fr'},
        { name: 'data_addr', value: 'https://ratemy2.changala.fr/data'},
        { name: 'url_begin', value: '/api/v'},
        { name: 'url_login', value: '/login'},
        { name: 'url_config', value: '/config'},
        { name: 'url_lang', value: '/lang'},
        { name: 'url_categories', value: '/categories'},
        { name: 'url_hiscores', value: '/p/hiscores'},
        { name: 'url_resources', value: '/p/resources'},
        { name: 'url_resources_views', value: '/p/resources/views'},
        { name: 'url_resources_votes', value: '/p/resources/votes'},
        { name: 'url_resources_shares', value: '/p/resources/shares'},
        { name: 'url_resources_favorites', value: '/p/resources/favorites'},
        { name: 'url_resources_reports', value: '/p/resources/reports'},
        { name: 'url_resources_comments', value: '/p/resources/comments'},
        { name: 'url_comments_votes', value: '/p/comments/votes'},
        { name: 'default_avatar', value: '/default_avatar.png'}
    ]).then(function() {
        return categories.bulkCreate([
            { name: 'Cats', image: '/img_cat/cat.jpg'},
            { name: 'Dogs', image: '/img_cat/dog.jpg'},
            { name: 'Cars', image: '/img_cat/car.jpg'},
        ]);
    });

}
var LE = require('letsencrypt-express');

    //server: 'staging',
var lex = LE.create({
    server: 'https://acme-v01.api.letsencrypt.org/directory',
    configDir: '/var/www/ratemy/ng/letsencrypt/etc',
    webrootPath: '/var/www/ratemy/ng/.well-known/acme-challenge',
    store: require('le-store-certbot').create({ webrootPath: '/var/www/ratemy/ng/.well-known/acme-challenge' }),
    approveDomains: ['ratemy2.changala.fr'],
    renewWithin: 10 * 24 * 60 * 60 *1000,
    renewBy: 5 * 24 * 60 * 60 * 1000,
    email: 'vincent.changala@gmail.com',
    agreeTos: true,
    debug: true,
    app: app
});
var shouldforce = false;
rymodels.sequelize.sync({force:shouldforce}).then(function(){
    if (shouldforce) init();
    console.log('init done');
    lex.listen();
    /*lex.listen([3000], [3001], function () {
          var protocol = ('requestCert' in this) ? 'https': 'http';
            console.log("Listening at " + protocol + '://localhost:' + this.address().port);
    });*/
});
/*
(function() {
        var childProcess = require("child_process");
            var oldSpawn = childProcess.spawn;
                function mySpawn() {
                            console.log('spawn called');
                                    console.log(arguments);
                                            var result = oldSpawn.apply(this, arguments);
                                                    return result;
                                                        }
                    childProcess.spawn = mySpawn;
})();
*/
