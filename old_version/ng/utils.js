function errhook(req, res, next) {
    return function(err) {
        res.tmprep.errmsg = JSON.stringify(err);
        return next('SQL_ERROR');
    }
}
function getFilesizeInBytes(filename) {
    var stats = fs.statSync(filename)
    var fileSizeInBytes = stats["size"]
    return fileSizeInBytes
}
function setWhereClause(where, key, data){
    if(data != null){
        where[key] = data;
    }
}

function setWhereClause2(data){
    if(data != null){
        return data;
    }
}

exports.setWhereClause = setWhereClause;
exports.setWhereClause2 = setWhereClause2;
exports.getFilesizeInBytes = getFilesizeInBytes;
exports.errhook = errhook;
