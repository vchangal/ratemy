var Sequelize = require('sequelize');
var crypto = require('crypto');
var sequelize = new Sequelize('ratemydb', 'ratemyadmin', 'ratemyadmin', {
    host: 'localhost',
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        idle: 10000
      },
});
var comments = sequelize.define('comments', {
    text: {
        type: Sequelize.STRING
    },
    updated: {
        type: Sequelize.INTEGER
    },
},{
    underscored: true,
});
var shares = sequelize.define('shares', {
    number: {
        type: Sequelize.INTEGER
    }
},{
    underscored: true
});
var views = sequelize.define('views', {
    number: {
        type: Sequelize.INTEGER
    }
},{
    underscored: true,
});
var votes = sequelize.define('votes', {
    note: {
        type: Sequelize.INTEGER
    }
},{
    underscored: true,
});
var banlists = sequelize.define('banlists', {
    uuid: {
        type: Sequelize.UUID
    }
},{
    underscored: true,
});


var users = sequelize.define('users', {
    username: {type: Sequelize.STRING(30)},
    password: {
        type: Sequelize.STRING(128),
        set: function(val){
            var hash = crypto.createHash('sha256').update(val).digest('base64');
            this.setDataValue('password', hash);
        }
    },
    uuid: {
        type: Sequelize.UUID,
        primaryKey: true
    },
    picture: {type: Sequelize.STRING},
    mail: {type: Sequelize.STRING}
},{
    underscored: true,
});

var tokens = sequelize.define('tokens', {
    username: {
        type: Sequelize.STRING(30)
    },
    token: {
        type: Sequelize.STRING(256)
    },
    time_token: {
        type: Sequelize.INTEGER(10)
    }
},{
    underscored: true,
});

var hiscores = sequelize.define('hiscores', {
    total: {
        type: Sequelize.VIRTUAL,
        get: function(){
            return this.getDataValue('score');
        }
    },
    score: {
        type: Sequelize.BIGINT,
        defaultValue: 0,
    },
    nb_votes: {
        defaultValue: 0,
        type: Sequelize.BIGINT},
    nb_views: {
        defaultValue: 0,
        type: Sequelize.BIGINT},
    nb_shares: {
        defaultValue: 0,
        type: Sequelize.BIGINT},
    nb_reports: {
        defaultValue: 0,
        type: Sequelize.BIGINT},
    nb_favorites: {
        defaultValue: 0,
        type: Sequelize.BIGINT},
    nb_comments: {
        defaultValue: 0,
        type: Sequelize.BIGINT},
    nb_resources: {
        defaultValue: 0,
        type: Sequelize.BIGINT},
},{
    underscored: true,
});
var resources = sequelize.define('resources', {
    type: {
        type: Sequelize.INTEGER
    },
    name: {
        type: Sequelize.STRING(50)
    },
    date: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    path: {
        type: Sequelize.STRING
    },
    mime: {
        type: Sequelize.CHAR(30)
    }
},{
    underscored: true,
});

var config = sequelize.define('configs', {
    name: {
        type: Sequelize.STRING(32)
    },
    value: {
        type: Sequelize.STRING
    }
},{
    underscored: true,
});
var lang = sequelize.define('langs', {
    key: {
        type: Sequelize.STRING(50)
    },
    value: {
        type: Sequelize.STRING(255)
    },
    lang: {
        type: Sequelize.STRING(5)
    },
    id2: {
        type: Sequelize.INTEGER
    }
},{
    underscored: true,
});

var favorites = sequelize.define('favorites', {
    /*
    user_id: {
        type: Sequelize.INTEGER
    },
    resource_id: {
        type: Sequelize.INTEGER
    },*/
},{
    underscored: true,
});
var comments_votes = sequelize.define('comments_votes', {
    note: {
        type: Sequelize.INTEGER
    }
},{
    underscored: true,
});
var reports = sequelize.define('reports', {
    text: {
        type: Sequelize.TEXT
    },
    reason: {
        type: Sequelize.INTEGER
    }
},{
    underscored: true,
});

var categories = sequelize.define('categories', {
    name: {
        type: Sequelize.STRING(50)
    },
    image: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    }
},{
    underscored: true,
});


/* Scopes */
categories.addScope('augmented', {
    attributes:{include:[[sequelize.fn('COUNT', sequelize.col('resources.id')), 'number']]},
    include:[
        {
            model: resources,
            attributes:[]
        }
    ],
    group: ['categories.id']

});
comments.addScope('augmented', {
            attributes:{
                include:[
                    [sequelize.fn('sum', sequelize.col('comments_votes.pos')), 'pos'],
                    [sequelize.fn('sum', sequelize.col('comments_votes.neg')), 'neg']
                ]
            },
    include:[
        {
            model: comments_votes,
            attributes: []
        }
    ],
    group: 'comments.id'
});

var includeDeclaration = {
    model: resources,
    attributes:{include:[
        [Sequelize.literal('AVG(distinct `resource.votes`.`note`)'), 'avg'],
        [Sequelize.literal('COUNT(distinct `resource.shares`.`id`)'), 'sum_shares'],
        [Sequelize.literal('COUNT(distinct `resource.views`.`id`)'), 'sum_views'],
        [Sequelize.literal('COUNT(distinct `resource.comments`.`id`)'), 'sum_comments'],
        [Sequelize.literal('`resource.user`.username'), 'username'],
        [Sequelize.literal('`resource.user`.picture'), 'picture'],
    ]},
    include: [
        {
            model: votes,
            attributes:[]
        },
        {
            model: views,
            attributes:[],
        },
        {
            model: comments,
            attributes:[],
        },
        {
            model: shares,
            attributes:[]
        },
        {
            model: users,
            attributes:[],
        }
    ],
};
favorites.addScope('augmented', {
    include:[
        includeDeclaration
    ],
    group: 'favorites.id'
});
shares.addScope('augmented', {
    include:[
        includeDeclaration
    ],
    group: 'shares.id'
});
hiscores.addScope('augmented', {
    attributes:{include: [
        [Sequelize.col('user.username'), 'username'],
        [Sequelize.col('user.picture'), 'picture'],
    ]},
    include:[
        {
            model: users,
            attributes:[],
        }
    ]
});

resources.addScope('augmented', {
        attributes:{include: [
            [Sequelize.literal('AVG(distinct votes.note)'), 'avg'],
            [Sequelize.literal('COUNT(distinct shares.id)'), 'sum_shares'],
            [Sequelize.literal('COUNT(distinct views.id)'), 'sum_views'],
            [Sequelize.literal('COUNT(distinct comments.id)'), 'sum_comments'],
            [Sequelize.col('user.uuid'), 'user_id'],
        ]},
        include: [
            {
                model: votes,
                attributes:[],
            },
            {
                model: views,
                attributes:[],
            },
            {
                model: comments,
                attributes:[],
            },
            {
                model: shares,
                attributes:[]
            },
            {
                model: users,
                attributes:[],
            }
        ],
        group: ['resources.id', 'user.uuid'],
});
users.addScope('augmented', {
    attributes:{
        exclude: ['mail', 'uuid', 'password'],
        include:[
            [Sequelize.literal('COUNT(distinct votes.id)'), 'nb_votes'],
            [Sequelize.literal('COUNT(distinct shares.id)'), 'nb_shares'],
            [Sequelize.literal('COUNT(distinct views.id)'), 'nb_views'],
            [Sequelize.literal('COUNT(distinct comments.id)'), 'nb_comments'],
            [Sequelize.literal('COUNT(distinct favorites.id)'), 'nb_favorites'],
            [Sequelize.literal('COUNT(distinct reports.id)'), 'nb_reports'],
            [Sequelize.literal('COUNT(distinct resources.id)'), 'nb_resources'],
        ]},
        include: [
            {
                model: votes,
                attributes:[],
            },
            {
                model: resources,
                attributes:[],
            },
            {
                model: views,
                attributes:[],
            },
            {
                model: comments,
                attributes:[],
            },
            {
                model: shares,
                attributes:[]
            },
            {
                model: favorites,
                attributes:[]
            },
            {
                model: reports,
                attributes:[]
            },
        ],
        group: ['users.uuid']
});


/* Relations */
 /* user <=> hiscores */
users.hasOne(hiscores, {foreignKey: 'user_id'});
hiscores.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> banlists */
users.hasOne(banlists, {foreignKey: 'user_id'});
banlists.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> tokens */
users.hasOne(tokens, {foreignKey: 'user_id'});
tokens.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> resources */
users.hasMany(resources, {foreignKey: 'user_id'});
resources.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> shares */
shares.belongsTo(users, {foreignKey: 'user_id'});
users.hasMany(shares, {foreignKey: 'user_id'});
 /* user <=> votes */
users.hasMany(votes, {foreignKey: 'user_id'});
votes.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> favorites */
users.hasMany(favorites, {foreignKey: 'user_id'});
favorites.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> comments */
users.hasMany(comments, {foreignKey: 'user_id'});
comments.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> comments_votes */
users.hasMany(comments_votes, {foreignKey: 'user_id'});
comments_votes.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> reports */
users.hasMany(reports, {foreignKey: 'user_id'});
reports.belongsTo(users, {foreignKey: 'user_id'});
 /* user <=> view */
users.hasMany(views, {foreignKey: 'user_id'});
views.belongsTo(users, {foreignKey: 'user_id'});
 /* comments <=> comments_votes */
comments.hasMany(comments_votes, {foreignKey: 'comment_id'});
comments_votes.belongsTo(comments, {foreignKey: 'comment_id'});

resources.belongsTo(categories, {foreignKey: 'category_id'});
categories.hasMany(resources, {foreignKey: 'category_id'});
resources.hasMany(votes, {foreignKey: 'resource_id'});
votes.belongsTo(resources, {foreignKey: 'resource_id'});
resources.hasMany(comments, {foreignKey: 'resource_id'});
comments.belongsTo(resources, {foreignKey: 'resource_id'});
resources.hasMany(favorites, {foreignKey: 'resource_id'});
favorites.belongsTo(resources, {foreignKey: 'resource_id'});
resources.hasMany(reports, {foreignKey: 'resource_id'});
reports.belongsTo(resources, {foreignKey: 'resource_id'});
resources.hasMany(shares, {foreignKey: 'resource_id'});
shares.belongsTo(resources, {foreignKey: 'resource_id'});
resources.hasMany(views, {foreignKey: 'resource_id'});
views.belongsTo(resources, {foreignKey: 'resource_id'});

comments.hasMany(reports, {foreignKey: 'comment_id'});
reports.belongsTo(comments, {foreignKey: 'comment_id'});

comments.hasMany(views, {foreignKey: 'comment_id'});
views.belongsTo(comments, {foreignKey: 'comment_id'});

/* hooks */
users.afterCreate(function(user, opts){
    return hiscores.create({
        user_id: user.uuid
    }, {transaction: opts.transaction}).then(function(data){
    });
});
comments.afterCreate(function(comment, opts){
    var p1 = hiscores.findOne({where:{
            user_id: comment.user_id
        }, transaction: opts.transaction
        }).then(function(user){
            user.nb_comments++;
            user.save();
        });
        var p2 = comment.getResource({transaction: opts.transaction}).then(function(res){
            return res.getUser({transaction: opts.transaction}).then(function(user){
                return hiscores.findOne({where:{
                    user_id: user.uuid
                }, transaction: opts.transaction
                }).then(function(user){
                    user.score += 2;
                    user.save();
                });
            });
        });
    return sequelize.Promise.all([p1, p2]);
});
comments.beforeDestroy(function(comment, opts){
    var p1 = hiscores.findOne({where:{
            user_id: comment.user_id
        },transaction: opts.transaction
        }).then(function(user){
            user.nb_comments--;
            user.save();
        });
    var p2 = comment.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score -= 2;
                user.save();
            });
        });
    });
    return sequelize.Promise.all([p1, p2]);
});
resources.afterCreate(function(resource, opts){
    return hiscores.findOne({where:{
        user_id: resource.user_id
    },transaction: opts.transaction
    }).then(function(user){
        user.nb_resources++;
        user.score += 10;
        user.save();
    });
});
resources.beforeDestroy(function(resource, opts){
    return hiscores.findOne({where: {
        user_id: resource.user_id
    },transaction: opts.transaction
    }).then(function(user){
        user.nb_resources--;
        user.score -= 10;
        user.save();
    });
});

favorites.afterCreate(function(favorite, opts){
    var p1 = hiscores.findOne({where:{
        user_id: favorite.user_id
    },transaction: opts.transaction
    }).then(function(user){
        user.nb_favorites++;
        user.save();
    });
    var p2 = favorite.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score += 5;
                user.save();
            });
        });
    });
    return sequelize.Promise.all([p1, p2]);
});
favorites.beforeDestroy(function(favorite, opts){
    var p1 = hiscores.findOne({where:{
        user_id: favorite.user_id
    },transaction: opts.transaction
    }).then(function(user){
        user.nb_favorites--;
        user.save();
    });
    var p2 = favorite.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score -= 5;
                user.save();
            });
        });
    });
    return sequelize.Promise.all([p1, p2]);
});
shares.afterCreate(function(share, opts){
    var p1 = hiscores.findOne({where:{
        user_id: share.user_id
    },transaction: opts.transaction
    }).then(function(user){
        user.nb_shares++;
        user.save();
    });
    var p2 = share.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score += 5;
                user.save();
            });
        });
    });
    return sequelize.Promise.all([p1, p2]);
});
shares.beforeDestroy(function(share, opts){
    var p1 = hiscores.findOne({where:{
        user_id: share.user_id
    },transaction: opts.transaction
    }).then(function(user){
        user.nb_shares--;
        user.save();
    });
    var p2 = share.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score -= 5;
                user.save();
            });
        });
    });
    return sequelize.Promise.all([p1, p2]);
});
views.afterCreate(function(view, opts){
    return view.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.nb_views++;
                user.save();
            });
        });
    });
});
views.beforeDestroy(function(view, opts){
    return view.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.nb_views--;
                user.save();
            });
        });
    });
});
reports.afterCreate(function(report, opts){
    return report.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.nb_reports++;
                user.save();
            });
        });
    });
});
reports.beforeDestroy(function(report, opts){
    return report.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.nb_reports--;
                user.save();
            });
        });
    });
});
votes.afterCreate(function(vote, opts){
    var p1 = hiscores.findOne({where:{
        user_id: vote.user_id
    },transaction: opts.transaction
    }).then(function(user){
        user.nb_votes++;
        user.save();
    });
    var p2 = vote.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score += vote.note;
                user.save();
            });
        });
    });
    return sequelize.Promise.all([p1, p2]);
});
votes.beforeUpdate(function(vote, opts){
    return vote.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score -= vote.note;
                user.save();
            });
        });
    });
});
votes.afterUpdate(function(vote, opts){
    return vote.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score += vote.note;
                user.save();
            });
        });
    });
});
votes.beforeDestroy(function(vote, opts){
    var p1 = hiscores.findOne({where:{
        user_id: vote.user_id
    },transaction: opts.transaction
    }).then(function(user){
        user.nb_votes--;
        user.save();
    });
    var p2 = vote.getResource({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score -= vote.note;
                user.save();
            });
        });
    });
    return sequelize.Promise.all([p1, p2]);
});

comments_votes.afterCreate(function(comments_vote, opts){
    return comments_vote.getComment({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score++;
                user.save();
            });
        });
    });
});
comments_votes.beforeUpdate(function(comments_vote, opts){
    return comments_vote.getComment({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score -= comments_vote.note;
                user.save();
            });
        });
    });
});
comments_votes.afterUpdate(function(comments_vote, opts){
    return comments_vote.getComment({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score += comments_vote.note;
                user.save();
            });
        });
    });
});
comments_votes.beforeDestroy(function(comments_vote, opts){
    return comments_vote.getComment({transaction: opts.transaction}).then(function(res){
        return res.getUser({transaction: opts.transaction}).then(function(user){
            return hiscores.findOne({where:{
                user_id: user.uuid
            },transaction: opts.transaction
            }).then(function(user){
                user.score--;
                user.save();
            });
        });
    });
});

/* exports */
exports.sequelize = sequelize;
exports.users = users;
exports.resources_shares = shares;
exports.resources_views = views;
exports.resources_votes = votes;
exports.hiscores = hiscores;
exports.resources = resources;
exports.banlist = banlists;
exports.tokens = tokens;
exports.categories = categories;
exports.config = config;
exports.lang = lang;
exports.favorites = favorites;
exports.comments = comments;
exports.comments_votes = comments_votes;
exports.reports = reports;
