<?php
require_once 'config.php';
require_once 'db.php';

function validate_filter_arrayexpected($app, $dft=true){
    $val = get_data($app, "arrayexpected", $dft);
    // $val = filter_var($val, FILTER_VALIDATE_BOOLEAN);
    if(is_null($val) || $val === $dft){
        return $dft;
    }
    else if($val === "true" ||
        $val === "True" ||
        $val == 1 ||
        $val === true){
            $val = true;
        }
    else{
        $val = false;

    }
    return $val;
}
function validate_filter_login($app, $dft=true){
    $val = get_data($app, "login", $dft);
    if($val == $dft){
        return $dft;
    }
    $bool = filter_var($val, FILTER_VALIDATE_BOOLEAN);
    return $bool;
}

function validate_filter_id($app, $dft=0){
    $val = get_data($app, "id", $dft);
    if($val == $dft){
        return $val;
    }
    if(!is_numeric($val)){
        save_data($app, 'msg', "id is not numeric", true);
        $app->send('NAN');
    }
    return $val;
}
function validate_filter_offset($app, $dft=0){
    $val = get_data($app, "offset", $dft);
    if($val == $dft){
        return $val;
    }
    if(!is_numeric($val)){
        save_data($app, 'msg', "offset is not numeric", true);
        $app->send('NAN');
    }
    $val = max(0, $val);
    return $val;
}

function validate_filter_limit($app, $dft=0){
    $limit = get_data($app, "limit", $dft);
    if($limit == $dft){
        return $limit;
    }
    if(!is_numeric($limit)){
        save_data($app, 'msg', "limit is not numeric", true);
        $app->send('NAN');
    }
    $limit = max(0, $limit);
    return $limit;
}
function validate_filter_type($app, $dft=0){
    $type = get_data($app, "type", $dft);
    if($type == $dft){
        return $type;
    }
    if(!is_numeric($type)){
        save_data($app, 'msg', "type is not numeric", true);
        $app->send('NAN');
    }
    $type = max(0, $type);
    return $type;
}

function validate_filter_rank($app, $dft=0){
    $rank = get_data($app, "rank", $dft);
    if($rank == $dft){
        return $rank;
    }
    if(!is_numeric($rank)){
        save_data($app, 'msg', "rank is not numeric", true);
        $app->send('NAN');
    }
    $rank = max(0, $rank);
    return $rank;
}
function validate_filter_sort($app, $dft='total'){
    $sort = get_data($app, "sort", $dft);
    $columns = [
        'total', 'user_id', 'username',
        'picture', 'nb_votes', 'nb_views',
        'nb_shares', 'nb_reports', 'nb_favorites',
        'nb_comments', 'nb_resources', 'score'
    ];
    if($sort == $dft){
        return $sort;
    }
    else if(!in_array($sort, $columns)){
        save_data($app, 'msg',
            "sort $msg is not possile for this table", true);
        $app->send('BAD_PARAM');
    }
    return $sort;
}

function validate_filter_username($app, $dft=null){
    $db = $app->get('db');
    $username = get_data($app, "username", $dft);
    if($username == $dft){
        return $dft;
    }else if(!is_string($username) ||
        strlen($username) > 30 ||
        strlen($username) < 4 ||
        !preg_match('/^\w+$/', $username)){
           $msg = "username is not alphanumeric or not within 5-29 characters";
           save_data($app, 'msg', $msg, true);
           $app->send('USERNAME');
    }
    return $username;
}
function validate_filter_uuid($app, $dft=""){
    $uuid = get_data($app, "uuid", $dft);
    if($uuid == $dft){
        return $uuid;
    }
    if(!is_string($uuid)){
        $msg = 'error in uuid';
        save_data($app, 'msg', $msg, true);
        $app->send('BAD_PARAM');
    }
    return $uuid;
}
function validate_filter_pass1($app, $dft=""){
    $pass1 = get_data($app, "pass1", $dft);
    if($pass1 == $dft){
        return $pass1;
    }
    if(!is_string($pass1)){
        $msg = 'error in password';
        save_data($app, 'msg', $msg, true);
        $app->send('USERNAME');
    }
    return $pass1;
}
function validate_filter_mail($app, $dft=""){
    $mail = get_data($app, "mail", $dft);
    $filtered = filter_var($mail, FILTER_VALIDATE_EMAIL);
    if($mail == $dft){
        return $mail;
    }
    if($filtered === false)
    {
        $msg = 'error in mail address';
        save_data($app, 'msg', $msg, true);
        $app->send('USERNAME');
    }
    return $mail;
}
function validate_filter_token($app, $dft=""){
    $val = get_data($app, "token", $dft);
    if($val == $dft){
        return $val;
    }
    if(!is_string($val)){
        $msg = "invalid token ($val)";
        save_data($app, 'msg', $msg, true);
        $app->send('TOKEN');
    }
    return $val;
}

function validate_filter_user_id($app, $dft=null){
    $db = $app->get('db');
    $user_id = get_data($app, 'user_id', $dft);
    $user_tmp_array = $user_id;
    if($user_tmp_array == $dft){
        return $dft;
    }
    if(!is_array($user_id)){
        $user_tmp_array = [$user_id];
    }
    $nb_users_bd = $db->count('users', ['id' => $user_tmp_array]);
    if($nb_users_bd != count($user_tmp_array)){
        $msg = "one of the users id is not available";
        save_data($app, 'msg', $msg, true);
        $app->send("RESOURCE_SELECT_BAD_PARAM");
    }
    return $user_tmp_array;
}
function validate_filter_user($app, $dft=null){
    $db = $app->get('db');
    $user = get_data($app, 'user', $dft);
    $user_tmp_array = $user;
    if($user_tmp_array == $dft){
        return $dft;
    }
    if(!is_array($user)){
        $user_tmp_array = [$user];
    }
    $nb_users_bd = $db->count('users', ['username' => $user_tmp_array]);
    if($nb_users_bd != count($user_tmp_array)){
        $msg = "one of the username provided is not available";
        save_data($app, 'msg', $msg, true);
        $app->send("RESOURCE_SELECT_BAD_PARAM");
    }
    return $user_tmp_array;
}
function validate_filter_resource_id($app, $dft=null){
    $db = $app->get('db');
    $resource_id = get_data($app, 'resource_id', $dft);
    $resource_tmp_array = $resource_id;
    if($resource_tmp_array == $dft){
        return $dft;
    }
    if(!is_array($resource_id)){
        $resource_tmp_array = [$resource_id];
    }
    $nb_resources_bd = $db->count('resources', ['id' => $resource_tmp_array]);
    if($nb_resources_bd != count($resource_tmp_array)){
        $msg = "one of the resources id ("
            .print_r($resource_tmp_array,true)
            ."is not available";
        save_data($app, 'msg', $msg, true);
        $app->send("RESOURCE_SELECT_BAD_PARAM");
    }
    return $resource_tmp_array;
}
function validate_filter_categories($app, $dft=null){
    $db = $app->get('db');
    $categories = get_data($app, 'categories', $dft);
    $resource_tmp_array = $categories;
    if($resource_tmp_array == $dft){
        return $dft;
    }
    if(!is_array($categories)){
        $resource_tmp_array = [$categories];
    }
    $nb_resources_bd = $db->count('categories', ['id' => $resource_tmp_array]);
    if($nb_resources_bd != count($resource_tmp_array)){
        $msg = "one of the categories id is not available";
        save_data($app, 'msg', $msg, true);
        $app->send("RESOURCE_SELECT_BAD_PARAM");
    }
    return $resource_tmp_array;
}

function validate_filter_comments_id($app, $dft=null){
    $db = $app->get('db');
    $comments_id = get_data($app, 'comments_id', $dft);
    $comments_tmp_array = $comments_id;
    if($comments_tmp_array == $dft){
        return $dft;
    }
    if(!is_array($comments_id)){
        $comments_tmp_array = [$comments_id];
    }
    $nb_commentss_bd = $db->count('comments', ['id' => $comments_tmp_array]);
    if($nb_commentss_bd != count($comments_tmp_array)){
        $msg = "one of the comments id is not available";
        save_data($app, 'msg', $msg, true);
        $app->send("RESOURCE_SELECT_BAD_PARAM");
    }
    return $comments_tmp_array;
}
function validate_filter_method($app, $dft='hiscores'){
    $method = get_data($app, "method", $dft);
    $columns = ['hiscores', 'random'];
    if($method == $dft){
        return $method;
    }
    else if(!in_array($method, $columns)){
        save_data($app, 'msg',
            "method $msg is not possile for this table", true);
        $app->send('BAD_PARAM');
    }
    return $method;
}

function validate_filter_comments($app, $dft=null){
    $comments = get_data($app, "comments", $dft);
    if($comments == $dft){
        return $comments;
    }
    if(!is_string($comments)){
        $msg = "comments is not a string";
        save_data($app, 'msg', $msg, true);
        $app->send('RESOURCE_INPUT');
    }
    $comments = truncate($comments, 255);
    return $comments;
}

function validate_filter_note($app, $dft=0){
    $note = get_data($app, "note", $dft);
    if($note == $dft){
        return $note;
    }
    if(!is_numeric($note)){
        $msg = "note is not numeric";
        save_data($app, 'msg', $msg, true);
        $app->send('VOTE');
    }
    $note = max(min(intval($note),1),-1);
    return $note;

}
function validate_filter_vote($app, $dft=0){
    $note = get_data($app, "note", $dft);
    if($note == $dft){
        return $note;
    }
    if(!is_numeric($note)){
        $msg = "note is not numeric";
        save_data($app, 'msg', $msg, true);
        $app->send('VOTE');
    }
    $note = max(min(intval($note),5),0);
    return $note;

}
function validate_filter_reason($app, $dft=0){
    $val = get_data($app, "reason", $dft);
    if($val == $dft){
        return $dft;
    }
    if(!is_numeric($val)){
        $msg = "reason is not numeric";
        save_data($app, 'msg', $msg, true);
        $app->send('BAD_PARAM');
    }
    return $val;
}

function validate_filter_name($app, $dft=null){
    $name = get_data($app, "name", $dft);
    if($name == $dft){
        return $name;
    }
    if(!is_string($name)){
        $msg = "name is not a string";
        save_data($app, 'msg', $msg, true);
        $app->send('RESOURCE_INPUT');
    }
    $name = truncate($name, 50);
    return $name;
}
function validate_filter_description($app, $dft=null){
    $description = get_data($app, "description", $dft);
    if($description == $dft){
        return $description;
    }
    if(!is_string($description)){
        $msg = "description is not a string";
        save_data($app, 'msg', $msg, true);
        $app->send('RESOURCE_INPUT');
    }
    $description = truncate($description, 255);
    return $description;
}
function validate_filter_file($app, $dft=null){
    $msg = "";
    $uploadedfile = $app->request()->files->file;
    if(empty($uploadedfile)){
       if(func_num_args() == 1) {
           save_data($app, 'msg', 'no file uploaded', true);
           $app->send('RESOURCE_INPUT');
       }
       else{
           return $dft;
       }
    }
    $file = $uploadedfile['tmp_name'];
    $error = $uploadedfile['error'];
    if(!isset($error) || is_array($error)){
        $msg = "Error in file parameters";
        save_data($app, 'msg', $msg, true);
        $app->send('RESOURCE_INPUT');
    }
    switch ($error) {
    case UPLOAD_ERR_OK:
        $return_value = True;
        break;
    case UPLOAD_ERR_NO_FILE:
        $msg = "No file sent";
        break;
    case UPLOAD_ERR_INI_SIZE:
    case UPLOAD_ERR_FORM_SIZE:
        $msg = "File is too big";
        break;
    default:
        $msg = "Unknown error";
        break;
    }
    if($error != UPLOAD_ERR_OK){
        save_data($app, 'msg', $msg, true);
        $app->send('RESOURCE_INPUT');
    }
    if(!is_uploaded_file($file)){
        $msg = "resource sended is not a valid file ".print_r($uploadedfile, true);
        save_data($app, 'msg', $msg, true);
        $app->send('RESOURCE_INPUT');
    }
    $size = filesize($file);
    if($size > 1024 * 1024 * 20){
        $msg = "resource size is too big";
        save_data($app, 'msg', $msg, true);
        $app->send('RESOURCE_INPUT');
    }
    return $file;
}
