<?php

function truncate($string, $limit, $break=".", $pad="...")
{
    // return with no change if string is shorter than $limit
    if(strlen($string) <= $limit) return $string;

    // is $break present between $limit and the end of the string?
    if(false !== ($breakpoint = strpos($string, $break, $limit))) {
        if($breakpoint < strlen($string) - 1) {
            $string = substr($string, 0, $breakpoint) . $pad;
        }
    }

    return $string;
}
function get_user_id($db, $username, $default=0){
    $result = $db->get("users", "id", ["username" => $username]);
    if($result === false){
        $result = $default;
    }
    return $result;
}
function get_data($app, $var, $dft=null){
    $request = $app->request();
    if(isset($request->data[$var])){
        return $request->data[$var];
    }
    else if(isset($request->query[$var])){
        return $request->query[$var];
    }
    else if(func_num_args() > 2){
        return $dft;
    }else{
        $msg = "missing parameter $var";
        error_log($msg);
        save_data($app, 'msg', $msg, true);
        $app->send('MISSING_PARAM');
    }
}

function get_answer($app, $key, $dft=null){
    $answer = $app->get('answer');
    if(array_key_exists($key, $answer)){
        return $answer[$key];
    }
    else{
        return $dft;
    }
}
function rm_from_answer($app, $key){
    $answer = $app->get('answer');
    unset($answer[$key]);
    $app->set('answer', $answer);
}

function save_data(&$app, $key, $var, $override = false){
    $answer = $app->get('answer');
    if(is_null($key)){
        $app->set('answer', $var);
    }
    else if($override === true){
        $answer[$key] = $var;
        $app->set('answer', $answer);
    }
    else if($override == "append"){
        $answer[$key] = $answer[$key] . $var;
        $app->set('answer', $answer);
    }
    else if($override == "prepend"){
        $answer[$key] = $var . $answer[$key];
        $app->set('answer', $answer);
    }
    else{
        if(!array_key_exists($key, $answer)){
            $answer[$key] = $var;
            $app->set('answer', $answer);
        }
        else if(is_array($answer[$key])){
            $answer[$key] = array_merge($answer[$key], $var);
            $app->set('answer', $answer);
        }
    }
    return $var;
}

function get_user_name($db, $id, $default=""){
    $result = $db->get("users", "username", ["id" => $id]);
    if($result === false){
        $result = $default;
    }
    return $result;
}

function get_category_name($db, $id, $default=""){
    $result = $db->get("categories", "name", ["id" => $id]);
    if($result === false){
        $result = $default;
    }
    return $result;
}
function get_resource_name($db, $id, $default=""){
    $result = $db->get("resources", "name", ["id" => $id]);
    if($result === false){
        $result = $default;
    }
    return $result;
}

function get_resource_path($db, $id, $default=""){
    $result = $db->select("resources", "path", ["id" => $id]);
    if($result === false){
        $result = $default;
    }
    return $result;
}

function validate_numeric($app, $key, &$status, $default=0, $is_opt=false){
    $value = get_data($app, $key, $default, $is_opt);
    if(!is_null($value) && !is_numeric($value)){
        $msg = "$key is not numeric";
        save_data($app, 'msg', $msg, true);
        $status = Config::$ERROR['NAN'];
        return False;
    }
    save_data($app, $key, $value);
    return True;
}

function answer($app, $status = 0, $status_code = 200){
    if($status_code != 200){
        save_data($app, 'status', $status);
    }
    $app->json($app->get('answer'), $status_code);
}
