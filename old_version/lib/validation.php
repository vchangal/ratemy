<?php
require_once 'config.php';
require_once 'db.php';

function validate_arrayexpected($app, $db, &$status){
    $arrayexpected = get_data($app, "arrayexpected", "true");
    $bool = filter_var($arrayexpected, FILTER_VALIDATE_BOOLEAN);
    save_query($app, 'arrayexpected', $bool);
    return True;
}

function validate_islogged($app, $db, &$status){
    $username = get_data($app, "username");
    $token = get_data($app, "token");
    $cond = ["username" => $username];
    $time_token = time();
    $correct = $db->has('tokens', $cond);
    if($correct){
        $last_time = $db->get('tokens',
            'time_token', $cond);
        if($time_token <= $last_time + Config::SESSION_TIMEOUT){
            save_data($app, 'my_username', $username);
            $user_id = get_user_id($db, $username);
            save_data($app, 'my_user_id', $user_id);
            $istokenvalid = $db->has('tokens', [
                "AND" => [
                    "username" => $username,
                    "token" => $token,
                ]
            ]);
            if($istokenvalid){
                /* FIXME temporary send the same token
                $newtoken = hash('sha512', $token); */
                $newtoken = $token;
                $time_token = time();
                $db->update('tokens',
                    ['token' => $newtoken,
                    'time_token' => $time_token],
                    ['username' => $username]);
                save_data($app, 'token', $newtoken);
                save_data($app, 'msg', 'token is valid', true);
                $status = Config::$ERROR['NONE'];
                return True;
            }
            else{
                $msg = "token freshness error";
                $status = Config::$ERROR['LOGIN_EXPIRED'];
            }
        }
        else{
            $msg = "invalid token";
            $status = Config::$ERROR['TOKEN'];
        }
    }
    else{
        $msg = "token error, user is not logged";
        $status = Config::$ERROR['NOT_LOGGED'];
    }
    save_data($app, 'msg', $msg, true);
    return False;
}

function file_upload($app, $uploadedfile, $newfilename, $destfolder){

    $msg = "";
    $return_value = True;
    if(empty($uploadedfile)) {
        save_data($app, 'msg', 'no file uploaded', true);
        $return_value = True;
    }else{
        $file = $uploadedfile['tmp_name'];
        $size = $uploadedfile['size'];
        $error = $uploadedfile['error'];
        if(!isset($error) || is_array($error)){
            $msg = "Error in file parameters";
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_INPUT'];
            $return_value = False;
        }
        else{
            $return_value = False;
            switch ($error) {
            case UPLOAD_ERR_OK:
                $return_value = True;
                break;
            case UPLOAD_ERR_NO_FILE:
                $msg = "No file sent";
                break;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $msg = "File is too big";
                break;
            default:
                $msg = "Unknown error";
                break;
            }
            if($return_value === False){
                $status = Config::$ERROR['RESOURCE_INPUT'];
                save_data($app, 'msg', $msg, true);
                return False;
            }else if(is_uploaded_file($file)){
                try{
                    $im = new Imagick($file);
                    $mime = extractMime($file);
                    $ext = getImageExtension($mime);
                    if($ext === False){
                        $status = Config::$ERROR['RESOURCE_INPUT'];
                        save_data($app, 'msg', "mime $mime not supported", true);
                        return False;
                    }
                    $im->setImageFormat('jpg');
                    $wwwpath = "/$destfolder/$newfilename.jpg";
                    $tmppath = $app->get('BASE_DIR').$wwwpath;
                    if($size > 512*1024){
                        $im->setCompression(50);
                    }
                    $im->writeImage($tmppath);
                    $mime = 'image/jpeg';
                    save_query($app, "profile", $wwwpath);
                    $return_value = True;
                }catch(ImagickException $e){
                    $msg = "Error in file $name conversion";
                    error_log($msg . " => ". $e->getMessage());
                    save_data($app, 'msg', $msg, true);
                    $status = Config::$ERROR['RESOURCE_INPUT'];
                    $return_value = False;
                }
            }
            else{
                $msg = "resource sended is not a valid file ".print_r($uploadedfile, true);
                $return_value = False;
            }
        }
    }
    save_data($app, 'msg', $msg, true);
    return $return_value;
}
function extractMime($filename){
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mime = finfo_file($finfo, $filename);
    finfo_close($finfo);
    return $mime;
}
function getImageExtension($mime_type){
    $value = False;
    $extensions = array('image/jpeg' => 'jpeg',
        'image/gif' => 'gif',
        'image/png' => 'png'
    );
    if(array_key_exists($mime_type, $extensions)){
        $value = $extensions[$mime_type];
    }
    return $value;
}
function validate_profile($app, $db, &$status){
    $msg = "";
    $returnvalue = False;
    $password = get_data($app, 'password', null);
    $username = get_data($app, 'username');
    $mail = get_data($app, 'mail', null);
    if(!is_null($password)){
        save_query($app, "password", $password);
    }
    if(!is_null($mail)){
        $filtered = filter_var($mail, FILTER_VALIDATE_EMAIL);
        if($filtered !== false)
        {
            save_query($app, "mail", $mail);
        }
        else{
            $msg = "Incorrect mail $mail";
            error_log($msg);
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_INPUT'];
            return False;
        }
    }
    if(file_upload($app, $app->request()->files->file, time().$username, "profiles")) {
        $returnvalue = True;
    }else{
        $status = Config::$ERROR['RESOURCE_INPUT'];
        $returnvalue = False;
    }
    return $returnvalue;
}

function validate_resource($app, $db, &$status){
    $msg = "";
    $cat = get_data($app, 'category');
    $type = get_data($app, 'type');
    $description = truncate(get_data($app, 'description', 'No description'), 255);
    $name = truncate(get_data($app, 'name'), 50);
    $correct = $db->has('categories', ['id' => $cat]);

    if(validate_type($app, $db, $status) === false){
        return false;
    }
    else if($correct){
        save_data($app, "category", $cat);
        save_data($app, "resource_description", $description);
        save_data($app, "resource_name", $name);
        save_data($app, "resource_type", $type);
        $file = $app->request()->files->file['tmp_name'];
        $size = $app->request()->files->file['size'];
        $tmppath = "";
        $mime = "";
        $ext = "";
        if(!isset($app->request()->files->file['error']) ||
            is_array($app->request()->files->file['error'])){
                $msg = "Error in  file parameters";
                error_log($msg);
                save_data($app, 'msg', $msg, true);
                $status = Config::$ERROR['RESOURCE_INPUT'];
                return False;
            }
        switch ($app->request()->files->file['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            $msg = "No file sent";
            error_log($msg);
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_INPUT'];
            return False;
            break;
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            $msg = "File is too big";
            error_log($msg);
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_INPUT'];
            return False;
            break;
        default:
            $msg = "Unknown error";
            error_log($msg);
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_INPUT'];
            return False;
            break;
        }
        if(is_uploaded_file($file)){
            switch ($type) {
            case 0:
                try{
                    $im = new Imagick($file);
                    $ext = 'jpg';
                    $im->setImageFormat($ext);
                    $tmppath = $app->get('BASE_DIR') . '/resources/'.time().".$ext";
                    if($size > 512*1024){
                        $im->setCompression(50);
                    }
                    $im->writeImage($tmppath);
                    $mime = 'image/jpeg';
                }catch(ImagickException $e){
                    $msg = "Error in file $name conversion";
                    error_log($msg . " => ". $e->getMessage());
                    save_data($app, 'msg', $msg, true);
                    $status = Config::$ERROR['RESOURCE_INPUT'];
                    return False;
                }
                break;
            case 1:
                $tmppath = $app->get('BASE_DIR') . '/resources/'.time().'.mp4';
                $returnvalue = 0;
                $output = array();
                exec("avconv -y -i $file -strict experimental -b 32k $tmppath", 
                    $output, $returnvalue);
                if($returnvalue){ //error
                    $msg = "Error in file $name conversion";
                    error_log($msg);
                    save_data($app, 'msg', $msg, true);
                    $status = Config::$ERROR['RESOURCE_INPUT'];
                    return False;
                }
                else{
                    $ext = 'mp4';
                    $mime = 'audio/mp4';
                }
                break;
            case 2:
                $tmppath = $app->get('BASE_DIR') . '/resources/'.time().'.webm';
                $returnvalue = 0;
                $output = array();
                exec("avconv -y -i $file -b 64k $tmppath",
                    $output, $returnvalue);
                if($returnvalue){ //error
                    $msg = "Error in file $name conversion";
                    error_log($msg);
                    save_data($app, 'msg', $msg, true);
                    $status = Config::$ERROR['RESOURCE_INPUT'];
                    return False;
                }
                else{
                    $ext = 'webm';
                    $mime = 'video/webm';
                }
                break;
            case 3:
                $tmppath = $file;
                $ext = 'txt';
                $mime = 'text/plain';
                if($size > 512*1024){
                    $msg = "Text file $name is way too long";
                    error_log($msg);
                    save_data($app, 'msg', $msg, true);
                    $status = Config::$ERROR['RESOURCE_INPUT'];
                    return False;
                }
                break;
            default:
                $msg = "Resource type $type not supported";
                error_log($msg);
                save_data($app, 'msg', $msg, true);
                $status = Config::$ERROR['RESOURCE_INPUT'];
                return False;
                break;
            }
            save_data($app, "type", $type);
            save_data($app, "resource", $tmppath);
            save_data($app, "resource_ext", $ext);
            save_data($app, "resource_mime", $mime);
            return True;
        }
        else{
            $msg = "resource sended is not a valid file ". print_r($app->request()->files->file, true);
        }
    }
    else{
        $msg = "category does not exist";
    }
    save_data($app, 'msg', $msg, true);
    $status = Config::$ERROR['RESOURCE_INPUT'];
    return False;
}
function validate_resource_id($app, $db, &$status){
    $username = get_data($app, 'username');
    $resource_id = get_data($app, 'resource_id');
    $user_id = get_user_id($db, $username);
    $msg = "";
    $correct = $db->has('resources', [
        "AND" => [
            'id' => $resource_id,
            'user_id' => $user_id
        ]
    ]);
    if($correct){
        $status = Config::$ERROR['NONE'];
        save_data($app, "username", $username);
        save_data($app, "resource_id", $resource_id);
        save_data($app, "user_id", $user_id);
        $msg = "resource id is checked";
    }
    else{
        $status = Config::$ERROR['RESOURCE_DELETE'];
        $msg = "resource is not available";
    }
    save_data($app, 'msg', $msg, true);
    return $correct;
}

function validate_comments_vote($app, $db, &$status){
    $note = get_data($app, 'note');
    $msg = "note and comments id are correct";
    if(!is_numeric($note)){
        $status = Config::$ERROR['VOTE'];
        $msg = "note is not numeric";
        return false;
    }
    else if(validate_comments_id_exist($app, $db, $status) === false){
        return false;
    }
    $note = max(min(intval($note),1),-1);
    save_data($app, 'note', $note);
    save_data($app, 'msg', $msg, true);
    return true;
}
function validate_vote($app, $db, &$status){
    $note = get_data($app, 'note');
    if(is_numeric($note)){
        $note = max(min(intval($note),5),0);
        save_data($app, 'note', $note);
        return validate_resource_exist($app, $db, $status);
    }
    $status = Config::$ERROR['VOTE'];
    $msg = "note is not numeric";
    save_data($app, 'msg', $msg, true);
    return False;
}

function validate_resource_exist($app, $db, &$status){
    $resource_id_array = null;
    $resource_id = get_data($app, "resource_id", null);
    if(!is_null($resource_id)){
        if(!is_array($resource_id)){
            $resource_id_array = [$resource_id];
        }else{
            $resource_id_array = $resource_id;
        }
        $nb_resource_id = $db->count('resources', ['id' => $resource_id_array]);
        if($nb_resource_id != count($resource_id_array)){
            $msg = "one of the resource id specified is not available";
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_SELECT_BAD_PARAM'];
            return False;
        }
    }
    $status = Config::$ERROR['NONE'];
    save_data($app, 'resource_id', $resource_id_array);
    return True;
}

function validate_comments_id_exist($app, $db, &$status){
    $comments_id = get_data($app, 'comments_id', null);
    $comments_tmp_array = $comments_id;
    if(!is_null($comments_id)){
        if(!is_array($comments_id)){
            $comments_tmp_array = [$comments_id];
        }
        $nb_comments_bd = $db->count('comments', ['id' => $comments_tmp_array]);
        if($nb_comments_bd != count($comments_tmp_array)){
            $msg = "one of the comments id is not available";
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_SELECT_BAD_PARAM'];
            return False;
        }
        save_data($app, 'comments_id', $comments_tmp_array);
    }
    $status = Config::$ERROR['NONE'];
    return True;
}
function validate_user_id_exist($app, $db, &$status){
    $user_id = get_data($app, 'user_id', null);
    $user_tmp_array = $user_id;
    if(!is_null($user_id)){
        if(!is_array($user_id)){
            $user_tmp_array = [$user_id];
        }
        $nb_users_bd = $db->count('users', ['id' => $user_tmp_array]);
        if($nb_users_bd != count($user_tmp_array)){
            $msg = "one of the users id is not available";
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_SELECT_BAD_PARAM'];
            return False;
        }
        save_data($app, 'user_id', $user_tmp_array);
    }
    $status = Config::$ERROR['NONE'];
    return True;
}
function validate_user_exist($app, $db, &$status){
    $user = get_data($app, 'user', null);
    $user_tmp_array = $user;
    if(!is_null($user)){
        if(!is_array($user)){
            $user_tmp_array = [$user];
        }
        $nb_users_bd = $db->count('users', ['username' => $user_tmp_array]);
        if($nb_users_bd != count($user_tmp_array)){
            $msg = "one of the users is not available";
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_SELECT_BAD_PARAM'];
            return False;
        }
        $user_id = get_user_id($db, $user_tmp_array);
        save_data($app, 'user_id', $user_id);
    }
    $status = Config::$ERROR['NONE'];
    return True;
}

function validate_category_exist($app, $db, &$status){
    $cat = get_data($app, 'categories');
    $cat_tmp_array = $cat;
    if(!is_null($cat)){
        if(!is_array($cat)){
            $cat_tmp_array = [$cat];
        }
        $nb_cat_bd = $db->count('categories', ['id' => $cat_tmp_array]);
        if($nb_cat_bd != count($cat_tmp_array)){
            $msg = "one of the categories is not available";
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['RESOURCE_SELECT_BAD_PARAM'];
            return False;
        }
    }
    save_data($app, 'categories', $cat);
    $status = Config::$ERROR['NONE'];
    return True;
}

function validate_type($app, $db, &$status){
    $type = get_data($app, "type", null);
    if(!is_null($type)){
        if(!is_numeric($type)){
            $msg = "$type is not numeric";
            save_data($app, 'msg', $msg, true);
            $status = Config::$ERROR['NAN'];
            return False;
        }
        else{
            $type = min(5,max(0,$type));
            save_data($app, 'type', $type);
        }
    }
    return true;
}

function validate_offset_limit($app, $db, &$status){
    if(!validate_numeric($app, 'offset', $status, 0)){
        return False;
    }
    else if(!validate_numeric($app, 'limit', $status, 1)){
        return False;
    }
    $status = Config::$ERROR['NONE'];
    return True;
}
