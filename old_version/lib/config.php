<?php

class Config
{

  const SESSION_TIMEOUT = 900; // 15mins en secondes
  public static $ERROR = [
      'NONE' => 0,
      'LOGIN' => 1,
      'REGISTER' => 2,
      'USERNAME' => 3,
      'LOGIN_EXPIRED' => 4,
      'NOT_LOGGED' => 5,
      'PASSWORD' => 6,
      'USERNAME_EXIST' => 7,
      'TOKEN' => 8,
      'ALREADY_LOGOUT' => 9,
      'RESOURCE' => 10,
      'RESOURCE_INPUT' => 11,
      'RESOURCE_DELETE' => 12,
      'RESOURCE_NOT_FOUND' => 13,
      'RESOURCE_SELECT_BAD_PARAM' => 14,
      'VOTE' => 15,
      'MISSING_PARAM' => 16,
      'VOTE_SELECT_BAD_PARAM' => 17,
      'BAD_PARAM' => 18,
      'NAN' => 19,
      'BANNED' => 20,
      'DB' => 21,
      'UNKNOWN' => 99
  ];

  public static $DB = [
    'database_type' => 'mysql',
    'database_name' => 'ratemydb',
    'server' => 'localhost',
    'username' => 'ratemyadmin',
    'password' => '94dS59aYVfdMqBSZ',
    'charset' => 'utf8'
];

  public static function err($str="UNKNOWN"){
      $_err = $str;
      if(!in_array($str, self::$ERROR)){
          $_err = "UNKNOWN";
      }
      return self::$ERROR[$_err];
  }

}
